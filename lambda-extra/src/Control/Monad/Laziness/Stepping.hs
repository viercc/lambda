{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE InstanceSigs #-}
module Control.Monad.Laziness.Stepping(
  -- * Re-export
  module Control.Monad.Laziness,
  
  Stepping(..), Ref(), Step(..), 
  diverge, tick,

  evalForever, evalInTime, Result(..)
) where

import Control.Monad.Laziness
import Control.Monad.ST.Strict
import Data.STRef.Strict
import Data.Bifunctor
import Control.Monad (ap, (<=<))
import Data.Foldable (traverse_)

data Step s a r where
  -- | Return with a value
  Return :: a -> Step s a r

  -- | Computation diverged
  Diverge :: Step s a r

  -- | One tick of a clock
  Tick :: r -> Step s a r

  -- | Force a delayed computation
  Force :: Ref s b -> (b -> r) -> Step s a r

deriving instance Functor (Step s a)
instance Bifunctor (Step s) where
  bimap f g step = case step of
    Return a -> Return (f a)
    Diverge -> Diverge
    Tick r -> Tick (g r)
    Force ref k -> Force ref (g . k)

newtype Stepping s a = Stepping { runStepping :: ST s (Step s a (Stepping s a)) }

newtype Ref s a = MkRef (STRef s (Stepping s a))
  deriving Eq

instance Functor (Stepping s) where
  fmap f =
    let go = Stepping . fmap (bimap f go) . runStepping
    in go

instance Applicative (Stepping s) where
  pure a = Stepping (pure (Return a))
  (<*>) = ap

instance Monad (Stepping s) where
  ma >>= k = Stepping $
    do step <- runStepping ma
       case step of
         Return a -> runStepping (k a)
         Diverge -> pure Diverge
         Tick next -> pure $ Tick (next >>= k)
         Force ref next -> pure $ Force ref (k <=< next)

instance MonadLaziness (Stepping s) where
  type Thunk (Stepping s) = Ref s
  
  delay :: Stepping s a -> Stepping s (Ref s a)
  delay ma = Stepping $ Return . MkRef <$> newSTRef ma
  
  force :: Ref s a -> Stepping s a
  force ref = Stepping $ pure $ Force ref pure
  
  delayFix :: (Ref s a -> Stepping s a) -> Stepping s (Ref s a)
  delayFix f = Stepping $ do
    ref <- newSTRef diverge
    let thunk = MkRef ref
    writeSTRef ref (f thunk)
    pure (Return thunk)
  
  delayFixMany :: Traversable t => (a -> t (Ref s b) -> Stepping s b) -> t a -> Stepping s (t (Ref s b))
  delayFixMany f as = Stepping $ do
    refs <- traverse (\a -> newSTRef diverge >>= \ref -> pure (a, ref)) as
    let thunks = MkRef . snd <$> refs
    traverse_ (\(a, ref) -> writeSTRef ref (f a thunks)) refs
    pure (Return thunks)

diverge :: Stepping s b
diverge = Stepping $ pure Diverge

tick :: Stepping s ()
tick = Stepping $ pure $ Tick (pure ())

data Result s a = Finished !Int a | Diverged | Remaining (Stepping s a)

evalInTime :: Int -> Stepping s a -> ST s (Result s a)
evalInTime time prog
  | time <= 0 = pure (Remaining prog)
  | otherwise = do
      steps <- runStepping prog
      case steps of
        Return a -> pure (Finished time a)
        Diverge -> pure Diverged
        Tick next -> evalInTime (time - 1) next
        Force (MkRef ref) body -> do
          innerProg <- readSTRef ref
          -- Having to force ref during evaluation of innerProg is infinite loop
          writeSTRef ref diverge
          result <- evalInTime time innerProg
          case result of
            Finished time' b -> do
              -- Write back the result
              writeSTRef ref (pure b)
              evalInTime time' (body b)
            Diverged -> pure Diverged
            Remaining innerProg' -> do
              writeSTRef ref innerProg'
              pure (Remaining prog)

evalForever :: Stepping s a -> ST s (Maybe a)
evalForever prog = do
  steps <- runStepping prog
  case steps of
    Return a -> pure (Just a)
    Diverge -> pure Nothing
    Tick next -> evalForever next
    Force (MkRef ref) body -> do
      innerProg <- readSTRef ref
      -- Having to force ref during evaluation of innerProg is infinite loop
      writeSTRef ref diverge
      result <- evalForever innerProg
      case result of
        Nothing -> pure Nothing
        Just b -> do
          -- Write back the result
          writeSTRef ref (pure b)
          evalForever (body b)
