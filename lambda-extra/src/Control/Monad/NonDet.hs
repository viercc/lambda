{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}
module Control.Monad.NonDet(
  NonDet(), randomChoice,
  NonDetCPS(), nondet,
  
  observeAll,
  observeOne,
  uncons, draw,
  randomUnconsBy, randomDrawBy
) where

import Data.Foldable (toList)
import Data.Monoid (First(..))
import Control.Applicative
import Control.Monad
import qualified Control.Monad.Fail as MonadFail

import qualified Data.Vector as V

data NonDet a = Fail | Success a | Choice (NonDet a) (NonDet a)
  deriving (Show, Eq, Ord, Functor, Foldable, Traversable)

instance Applicative NonDet where
  pure = Success
  (<*>) = ap

instance Monad NonDet where
  Fail         >>= _ = Fail
  Success a    >>= k = k a
  Choice m1 m2 >>= k = Choice (m1 >>= k) (m2 >>= k)

instance Alternative NonDet where
  empty = Fail
  (<|>) = Choice

instance MonadPlus NonDet where

instance MonadFail.MonadFail NonDet where
  fail _ = Fail

-- | 
newtype NonDetCPS a = ND
  { runNonDetCPS :: forall r. r -> (r -> r -> r) -> (a -> r) -> r }
  deriving (Functor)

instance Applicative NonDetCPS where
  pure a = ND $ \_ _ sk -> sk a
  (<*>) = ap

instance Monad NonDetCPS where
  ND m1 >>= k = ND $ \fk ck sk ->
    m1 fk ck $ \a -> runNonDetCPS (k a) fk ck sk

instance Alternative NonDetCPS where
  empty = ND $ \fk _ _ -> fk
  ND m1 <|> ND m2 = ND $ \fk ck sk -> ck (m1 fk ck sk) (m2 fk ck sk)

instance MonadPlus NonDetCPS

instance MonadFail.MonadFail NonDetCPS where
  fail _ = mzero

nondet :: NonDetCPS a -> NonDet a
nondet ma = runNonDetCPS ma Fail Choice Success

randomChoice :: (Alternative f) => [f a] -> f a
randomChoice fas = randomChoiceV (V.fromList fas)

randomChoiceV :: (Alternative f) => V.Vector (f a) -> f a
randomChoiceV fas
  | n == 0 = empty
  | n == 1 = V.head fas
  | otherwise =
      let (firstHalf, lastHalf) = V.splitAt (n `div` 2) fas
      in randomChoiceV firstHalf <|> randomChoiceV lastHalf
  where
    n = V.length fas

observeAll :: NonDet a -> [a]
observeAll = toList

observeOne :: NonDet a -> Maybe a
observeOne = getFirst . foldMap (First . Just)

uncons :: NonDet a -> Maybe (a, NonDet a)
uncons Fail           = Nothing
uncons (Success a)    = Just (a, Fail)
uncons (Choice Fail m2) = uncons m2
uncons (Choice (Success a) m2) = Just (a, m2)
uncons (Choice (Choice m1 m2) m3) = uncons (Choice m1 (Choice m2 m3))

draw :: Int -> NonDet a -> ([a], NonDet a)
draw n ma
  | n <= 0    = ([], ma)
  | otherwise = case uncons ma of
      Nothing -> ([], Fail)
      Just (a, ma') -> case draw (n-1) ma' of
        ~(as, ma'') -> (a:as, ma'')

compress :: NonDet a -> NonDet a
compress (Choice Fail x) = compress x
compress (Choice x Fail) = compress x
compress x = x

rebuild :: [NonDet a] -> NonDet a
rebuild [] = Fail
rebuild [x] = x
rebuild (Fail : Fail : rest) = rebuild (Fail : rest)
rebuild (x : y : rest) = rebuild (Choice x y : rest)

randomUnconsBy :: Monad m => m Bool -> NonDet a -> m (Maybe (a, NonDet a))
randomUnconsBy gen = go [] . compress
  where
    swapRandom (x,y) = do
      b <- gen
      pure (if b then (x,y) else (y,x))
    
    go []       Fail = return Nothing
    go (c:rest) Fail = go rest c 
    go crumbs   (Success a)    = return $ Just (a, rebuild crumbs)
    go crumbs   (Choice m1 m2) =
      do (m1,m2) <- swapRandom (m1,m2)
         go (m2:crumbs) m1

randomDrawBy :: Monad m => m Bool -> Int -> NonDet a -> m ([a], NonDet a)
randomDrawBy gen = loop []
  where
    loop acc n ma
      | n <= 0    = return (acc, ma)
      | otherwise = randomUnconsBy gen ma >>= \case
          Nothing       -> return (acc, Fail)
          Just (a, ma') -> loop (a:acc) (n-1) ma'
