{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveAnyClass #-}
module Control.Monad.Laziness(
  MonadLaziness(..),

  delayFixFromMonadFix,

  ThunkData(..),
  ThunkST(..), ThunkIO(..)
) where

import Control.Monad.Identity
import Control.Monad.Fix
import Data.Kind (Type)
import Control.Exception (Exception, throw, throwIO)
import Data.Coerce (coerce)

import Data.IORef

import Control.Monad.ST.Strict
import Data.STRef

class Monad m => MonadLaziness m where
  type Thunk m :: Type -> Type

  produce :: a -> m (Thunk m a)
  produce = delay . pure
  
  delay :: m a -> m (Thunk m a)
  force :: Thunk m a -> m a

  -- produce a >>= force === pure a

  -- produce a >> mb === mb

  -- delay ma >>= force === ma

  -- delay ma >>= \ref -> force ref >>= \a -> pure (ref, a)
  --   ===
  -- ma >>= \a -> produce a >>= \ref -> pure (ref, a)

  -- force ref >> force ref = force ref

  delayFix :: (Thunk m a -> m a) -> m (Thunk m a)
  delayFix f = runIdentity <$> delayFixMany (\_ refs -> f (runIdentity refs)) (Identity ())

  delayFixMany :: (Traversable t) => (a -> t (Thunk m b) -> m b) -> t a -> m (t (Thunk m b))
  default delayFixMany :: (MonadFix m, Traversable t) => (a -> t (Thunk m b) -> m b) -> t a -> m (t (Thunk m b))
  delayFixMany defFn as =
    mfix $ \refs -> traverse (\a -> delay (defFn a refs)) as

-- | 'delayFix' using 'MonadFix'
delayFixFromMonadFix :: (MonadLaziness m, MonadFix m) => (Thunk m a -> m a) -> m (Thunk m a)
delayFixFromMonadFix f = mfix (delay . f)

-- * Common exceptions that can occur during evaluation

data InfiniteLoop = InfiniteLoop
    deriving stock (Show, Eq, Ord)
    deriving anyclass (Exception)

data TooManyThunks = TooManyThunks
    deriving stock (Show, Eq, Ord)
    deriving anyclass (Exception)

data ForceStackOverflow = ForceStackOverflow
    deriving stock (Show, Eq, Ord)
    deriving anyclass (Exception)

-- | Using Haskell's builtin laziness
instance MonadLaziness Identity where
  type Thunk Identity = Identity
  produce = coerce
  delay = coerce
  force = coerce

-- | Internal representation of explicit thunk.
data ThunkData m a = Complete a | Busy | Later !(m a)

-- | Using STRef
instance MonadLaziness (ST s) where
  type Thunk (ST s) = ThunkST s
  
  produce a = UnsafeThunkST <$> newSTRef (Complete a)
  delay ma = UnsafeThunkST <$> newSTRef (Later ma)
  force (UnsafeThunkST ref) = do
    thunk <- readSTRef ref
    case thunk of
      Complete a -> pure a
      Busy -> throw InfiniteLoop
      Later ma -> do
        writeSTRef ref Busy
        a <- ma
        writeSTRef ref (Complete a)
        pure a

-- | Explicit thunk of ST computation (backed by STRef)
newtype ThunkST s a = UnsafeThunkST { unsafeGetThunkST :: STRef s (ThunkData (ST s) a) }
    deriving Eq

-- | Using IORef
instance MonadLaziness IO where
  type Thunk IO = ThunkIO

  produce a = UnsafeThunkIO <$> newIORef (Complete a)
  delay ma = UnsafeThunkIO <$> newIORef (Later ma)
  force (UnsafeThunkIO ref) = do
    thunk' <- takeLater ref
    case thunk' of
      Complete a -> pure a
      Busy -> throwIO InfiniteLoop
      Later ma -> do
        a <- ma
        atomicWriteIORef ref (Complete a)
        pure a
    where
      -- Atomically replace (Later _) with Busy,
      -- while returning the original ThunkData
      takeLater :: IORef (ThunkData m a) -> IO (ThunkData m a)
      takeLater ref = atomicModifyIORef' ref $ \thunk ->
        case thunk of
          Complete _ -> (thunk, thunk)
          Busy -> (thunk, thunk)
          Later _ -> (Busy, thunk)

-- | Explicit thunk of IO computation (backed by 'IORef')
newtype ThunkIO a = UnsafeThunkIO { unsafeGetThunkIO :: IORef (ThunkData IO a) }
    deriving Eq
