{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Lambda.LazyEval
  ( WHNF (..),
    Value (..),
    forceValue, produceValue,
    apply', apply,

    eval,
    evalStepping
  )
where

import Control.Monad.Laziness
import Control.Monad.Laziness.Stepping
import Data.Lambda.Core

-- * Lazy evaluation

data WHNF ann m c
  = LamHead' !ann !(Value ann m c -> m (WHNF ann m c))
  | ConHead' c [Value ann m c]

data Value ann m c
  = LamHead !ann !(Value ann m c -> m (WHNF ann m c))
  | ConHead c [Value ann m c]
  | ThunkValue !ann (Thunk m (WHNF ann m c))

forceValue :: (MonadLaziness m) => Value ann m c -> m (WHNF ann m c)
forceValue (LamHead ann f) = pure (LamHead' ann f)
forceValue (ConHead c args) = pure (ConHead' c args)
forceValue (ThunkValue _ thunk) = force thunk

produceValue :: WHNF ann m c -> Value ann m c
produceValue (LamHead' ann f) = LamHead ann f
produceValue (ConHead' c args) = ConHead c args

apply' :: (MonadLaziness m) => WHNF ann m c -> Value ann m c -> m (WHNF ann m c)
apply' (LamHead' _ f) y = f y
apply' (ConHead' c args) y = pure (ConHead' c (y : args))

apply :: (MonadLaziness m) => WHNF ann m c -> Value ann m c -> m (Value ann m c)
apply (LamHead' ann f) y = ThunkValue ann <$> delay (f y)
apply (ConHead' c args) y = pure $ ConHead c (y : args)

eval ::
  forall v l a x t m.
  (MonadLaziness m) =>
  l ->
  m () ->
  (a -> WHNF l m t) ->
  (x -> WHNF l m t) ->
  Lam v l a x ->
  m (Value l m t)
eval locDefault tick handleLit handleFV = \e -> ev e locDefault []
  where
    ev' :: Lam v l a x -> l -> [[Value l m t]] -> m (WHNF l m t)
    ev' (Lam fe) l env = case fe of
      App e1 e2 -> do
        tick
        v1 <- ev' e1 l env
        v2 <- ev e2 l env
        apply' v1 v2
      Abs _ e1 ->
        pure $ LamHead' l (\x -> ev' (unScope e1) l ([x] : env))
      Let defs body -> do
        tick
        defThunks <- traverse (\(_, rhs) -> ThunkValue l <$> delay (ev' rhs l env)) defs
        ev' (unScope body) l (defThunks : env)
      LetRec defs body -> do
        tick
        defRefs <-
          delayFixMany
            ( \(_, rhs) defRefs' ->
                let env' = map (ThunkValue l) defRefs' : env
                 in ev' (unScope rhs) l env'
            )
            defs
        ev' (unScope body) l (map (ThunkValue l) defRefs : env)
      BV n m -> forceValue $ (env !! n) !! m
      FV x -> pure $ handleFV x
      Lit a -> pure $ handleLit a
      Loc l' e -> ev' e l' env

    ev :: Lam v l a x -> l -> [[Value l m t]] -> m (Value l m t)
    ev (Lam fe) l env = case fe of
      App e1 e2 -> fmap (ThunkValue l) $ delay $ do
        tick
        v1 <- ev' e1 l env
        v2 <- ev e2 l env
        apply' v1 v2
      Abs _ e1 ->
        pure $ LamHead l (\x -> ev' (unScope e1) l ([x] : env))
      Let defs body -> do
        tick
        defThunks <- traverse (\(_, rhs) -> ThunkValue l <$> delay (ev' rhs l env)) defs
        ev (unScope body) l (defThunks : env)
      LetRec defs body -> do
        tick
        defRefs <-
          delayFixMany
            ( \(_, rhs) defRefs' ->
                let env' = map (ThunkValue l) defRefs' : env
                 in ev' (unScope rhs) l env'
            )
            defs
        ev (unScope body) l (map (ThunkValue l) defRefs : env)
      BV n m -> pure $ (env !! n) !! m
      FV x -> pure $ produceValue $ handleFV x
      Lit a -> pure $ produceValue $ handleLit a
      Loc l' e -> ev e l' env

evalStepping ::
  forall v l a x t s. 
  l ->
  (a -> WHNF l (Stepping s) t) ->
  (x -> WHNF l (Stepping s) t) ->
  Lam v l a x ->
  Stepping s (Value l (Stepping s) t)
evalStepping locDefault = eval locDefault tick
