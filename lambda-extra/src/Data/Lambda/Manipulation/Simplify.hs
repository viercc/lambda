{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE GADTs                      #-}
module Data.Lambda.Manipulation.Simplify (
  module Data.Lambda.Manipulation.Measure,

  ShrinkBetaConfig(..), defaultShrinkBetaConfig,
  shrinkBeta, customShrinkBeta,

  InlineConfig(..), defaultInlineConfig,
  inlineLet, customInlineLet,
) where

import           Control.Monad (join)

import           Data.Monoid
import           Data.Map                      (Map)
import qualified Data.Map.Strict               as Map
import qualified Data.MultiSet                 as MultiSet

import           Data.Lambda.Core
import           Data.Lambda.Manipulation.Util
import           Data.Lambda.Manipulation.Measure

data ShrinkBetaConfig a x =
  ShrinkBetaConfig {
    betaIter    :: Int,
    betaThresh1 :: Int,
    betaThresh2 :: Int,
    betaMeasure :: LamMeasure a x
  }

shrinkBeta :: Lam v l a x -> Lam v l a x
shrinkBeta = customShrinkBeta defaultShrinkBetaConfig

defaultShrinkBetaConfig :: ShrinkBetaConfig a x
defaultShrinkBetaConfig =
  ShrinkBetaConfig {
    betaIter = 1000,
    betaThresh1 = 20,
    betaThresh2 = 1000,
    betaMeasure = defaultLamMeasure
  }

customShrinkBeta :: ShrinkBetaConfig a x -> Lam v l a x -> Lam v l a x
customShrinkBeta ShrinkBetaConfig{..} =
    cata $ \fe -> oneLayer (Lam fe)
  where
    measure = measureLam betaMeasure
    oneLayer t = loop betaIter (measure t) t t
    loop k minCost t0 t | k <= 0    = t0
                        | otherwise =
      case nfStep measure betaThresh1 t of
        Nothing -> t0
        Just t'
          | minCost >= measure t'               -> loop (k-1) (measure t') t' t'
          | minCost + betaThresh2 >= measure t' -> loop (k-1) minCost t0 t'
          | otherwise                           -> t0

type Env v l a x = [[Maybe (Lam v l a x)]]

nfStep :: (Lam v l a x -> Int) -> Int ->
          Lam v l a x -> Maybe (Lam v l a x)
nfStep measure threshold = postprocess . go []
  where
    go env t = case whnfStep env t of
      Nothing -> goDown env t
      Just t' | measure t + threshold >= measure t' -> (Any True, t')
              | otherwise                           -> (Any False, t)

    postprocess (Any True, t) = Just t
    postprocess _             = Nothing

    goDown env (Lam fe) = case fe of
      App e1 e2          -> liftA2 app_ (go env e1) (go env e2)
      Abs v (Scope body) ->
        let env' = [Nothing] : env
        in abs_ v . Scope <$> go env' body
      Let defs (Scope body) ->
        let env' = (Just . snd <$> defs) : env
            defs' = (traverse . traverse) (go env) defs
            body' = go env' body
        in let_ <$> defs' <*> (Scope <$> body')
      LetRec defs (Scope body) ->
        let env' = (Nothing <$ defs) : env
            defs' = (traverse . traverse . traverse) (go env') defs
            body' = go env' body
        in letrec_ <$> defs' <*> (Scope <$> body')
      FV x -> (Any False, fv_ x)
      BV d i -> (Any False, bv_ d i)
      Lit a  -> (Any False, lit_ a)
      Loc l e -> loc_ l <$> go env e

whnfStep :: Env v l a x -> Lam v l a x -> Maybe (Lam v l a x)
whnfStep = go
  where
    (!?) :: [a] -> Int -> Maybe a
    xs !? n = case drop n xs of
      (x:_) -> Just x
      []    -> Nothing

    safeIdx :: [[Maybe a]] -> Int -> Int -> Maybe a
    safeIdx env d i = join $ (env !? d) >>= (!? i)

    go env (Lam fe) = case fe of
      App e1 e2 -> go' env e1 e2
      Abs _ _   -> Nothing
      Let defs (Scope body) ->
        let env' = (Just . snd <$> defs) : env
        in let_ defs . Scope <$> go env' body
      LetRec defs (Scope body) ->
        let env' = (Nothing <$ defs) : env
        in letrec_ defs . Scope <$> go env' body
      FV _ -> Nothing
      BV d i -> shift (d+1) <$> safeIdx env d i
      Lit _ -> Nothing
      Loc l e -> loc_ l <$> go env e

    go' env (Lam fe1) e2 = case fe1 of
      App e11 e12 -> (`app_` e2) <$> go' env e11 e12
      Abs _ body -> Just $ instantiate body [e2]
      Let defs (Scope body) ->
        let env' = (Just . snd <$> defs) : env
        in let_ defs . Scope <$> go' env' body (shift 1 e2)
      LetRec defs (Scope body) ->
        let env' = (Nothing <$ defs) : env
        in letrec_ defs . Scope <$> go' env' body (shift 1 e2)
      FV _ -> Nothing
      BV d i -> safeIdx env d i >>= \e1' -> go' env (shift (d+1) e1') e2
      Lit _ -> Nothing
      Loc l e1' -> loc_ l <$> go' env e1' e2

-- | Compactify Let and LetRec in lambda term
data InlineConfig a x =
  InlineConfig
    { inlineThreshold :: Int
    , inlineMeasure   :: LamMeasure a x
    }

defaultInlineConfig :: InlineConfig a x
defaultInlineConfig =
  InlineConfig {
    inlineThreshold=6,
    inlineMeasure=defaultLamMeasure
  }

inlineLet :: (Ord v) => Lam v l a x -> Lam v l a x
inlineLet = customInlineLet defaultInlineConfig

customInlineLet :: (Ord v) =>
  InlineConfig a x -> Lam v l a x -> Lam v l a x
customInlineLet config = cata step
  where
    step (Let lets body)    = toLet $ shrink $ fromLet lets body
    step (LetRec lets body) = toLetRec $ shrink $ fromLetRec lets body
    step e                  = Lam e

    shrink = inlineOnce . inlineSmall config

-- inline small definitions
inlineSmall :: InlineConfig a x -> LetForm v l a x -> LetForm v l a x
inlineSmall iConfig (LetForm toV letsMap body) = LetForm toV letsMap' body'
  where
    measure = inlineMeasure iConfig
    measure' = measure{measureFV = either (const 1) (measureFV measure)}

    isInlineable v e =
      countOV v e == 0 &&
      measureLam measure' e <= inlineThreshold iConfig

    stepSmall (v,e) (vs, f) =
      let e' = applySubst f e
      in if isInlineable v e' then (v:vs, (v ==> e') `compose` f) else (vs, f)

    (deletedVars, subst) = foldr stepSmall ([],emptySubst) $ Map.toList letsMap
    letsMap' = Map.map (applySubst subst) $
               foldr Map.delete letsMap deletedVars
    body' = applySubst subst body

-- substitute definitions that is used only once or is never used
inlineOnce :: LetForm v l a x -> LetForm v l a x
inlineOnce (LetForm toV letsMap body) = LetForm toV letsMap' body'
  where
    occurences = foldMap openedVarsBag letsMap <> openedVarsBag body

    stepOnce (v,e) (vs, f) =
      case MultiSet.occur v occurences of
        0 -> (v:vs, f)
        1 -> (v:vs, (v ==> applySubst f e) `compose` f)
        _ -> (vs, f)
    (deletedVars, subst) =
      foldr stepOnce ([], emptySubst) $ Map.toList letsMap
    letsMap' = Map.map (applySubst subst) $
                 foldr Map.delete letsMap deletedVars
    body' = applySubst subst body

-- * Substitution
type Subst k m x = Map k (m (Either k x))

(==>) :: (Eq k) => k -> m (Either k x) -> Subst k m x
(==>) = Map.singleton

compose :: (Ord k, Monad m) => Subst k m x -> Subst k m x -> Subst k m x
compose f g = Map.union f (Map.map (applySubst f) g)

emptySubst :: Subst k m x
emptySubst = Map.empty

applySubst :: (Ord k, Monad m) => Subst k m x -> m (Either k x) -> m (Either k x)
applySubst f t = t >>= either (\k -> Map.findWithDefault (pure (Left k)) k f) (pure . Right)

-- * Utilities

openedVarsBag :: (Ord k) => OLam k v l a x -> MultiSet.MultiSet k
openedVarsBag = foldMapOVs MultiSet.singleton
