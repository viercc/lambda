{-# LANGUAGE RecordWildCards #-}
module Data.Lambda.Manipulation.Measure(
    LamMeasure(..), defaultLamMeasure, measureLam
) where

import Data.Lambda.Core
import Data.Foldable (foldl')

data LamMeasure a x =
  LamMeasure
    { penaltyApp    :: Int
    , penaltyAbs    :: Int
    , penaltyLet    :: Int
    , penaltyLetRec :: Int
    , measureFV     :: x -> Int
    , measureBV     :: Int -> Int -> Int
    , measureLit    :: a -> Int
    }

defaultLamMeasure :: LamMeasure a x
defaultLamMeasure =
  LamMeasure{
    penaltyApp = 0,
    penaltyAbs = 1,
    penaltyLet = 3,
    penaltyLetRec = 6,
    measureFV = const 1,
    measureBV = \_ _ -> 1,
    measureLit = const 1
  }

measureLam :: LamMeasure a x -> Lam v l a x -> Int
measureLam LamMeasure{..} = cata step where
  sum' = foldl' (+) 0

  step fe =
    case fe of
      App e1 e2        -> penaltyApp + e1 + e2
      Abs _ body       -> penaltyAbs + unScope body
      Let lets body    -> penaltyLet + sum' (map snd lets) + unScope body
      LetRec lets body -> penaltyLetRec + sum' (map (unScope . snd) lets)
                                        + unScope body
      FV x             -> measureFV x
      BV d i           -> measureBV d i
      Lit a            -> measureLit a
      Loc _ e          -> e
