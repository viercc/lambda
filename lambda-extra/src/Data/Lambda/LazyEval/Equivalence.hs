{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables       #-}
module Data.Lambda.LazyEval.Equivalence (
  equiv,
  Val
) where

import           Control.Monad.ST

import           Data.Lambda.LazyEval
import Data.List.Infinite (Infinite(..))
import Control.Monad.Laziness.Stepping

import Data.Sequence (Seq(Empty, (:<|), (:|>)))

data Confidence = Confident | Unconfident

type Val l s a = Stepping s (WHNF l (Stepping s) a)

equiv :: (Eq a) => Int -> Int -> Infinite a -> Val l s a -> Val l s a -> ST s (Maybe Bool)
equiv calcLimit expandLimit constants x0 y0 =
    checkBFS constants Confident expandLimit Empty [(x0,y0)]
  where
    checkBFS as confidence budget queue (node : nodes)
      | budget <= 0 = pure Nothing
      | otherwise   = checkBFS as confidence (budget - 1) (queue :|> node) nodes
    checkBFS as confidence budget ((x, y) :<| queue) [] = do
      xResult <- evalInTime calcLimit x
      yResult <- evalInTime calcLimit y
      case (xResult, yResult) of
        -- Any one is Remaining: can't be sure
        (Remaining _, _) -> checkBFS as Unconfident budget queue []
        (_, Remaining _) -> checkBFS as Unconfident budget queue []

        -- Both Finished: go step
        (Finished _ x', Finished _ y') -> case step as x' y' of
          Nothing -> pure (Just False)
          Just (as', children) -> checkBFS as' confidence budget queue children
        
        -- Both Diverged: equal
        (Diverged, Diverged) -> checkBFS as confidence budget queue []

        -- Diverged versus Finished: inequal
        _ -> pure (Just False)
    checkBFS _ confidence _ Empty [] = case confidence of
      Unconfident -> pure Nothing
      Confident -> pure (Just True)

step :: (MonadLaziness m, Eq a) => Infinite a -> WHNF l m a -> WHNF l m a -> Maybe (Infinite a, [(m (WHNF l m a), m (WHNF l m a))])
step as (LamHead' _ f) (LamHead' _ g) =
  let a :< as' = as
      arg = ConHead a []
      x' = f arg
      y' = g arg
  in Just (as', [(x',y')])
step as (ConHead' x xargs) (ConHead' y yargs) =
  let eq = x == y && length xargs == length yargs
      xargs' = forceValue <$> xargs
      yargs' = forceValue <$> yargs
      children = zip xargs' yargs'
  in if eq then Just (as, children) else Nothing
step _ _ _ = Nothing
