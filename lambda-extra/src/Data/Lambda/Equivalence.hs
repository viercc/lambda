{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleInstances #-}
-- | Alpha-equivalence
module Data.Lambda.Equivalence (
  alphaEquiv
) where

import           Control.Monad

import           Data.Bits
import qualified Data.Foldable          as F
import           Data.List              (sortOn, sort)
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map
import           Data.Maybe             (maybeToList)

import           Data.Lambda.Core

-- | Decide alpha-equivalence of lambda term.
--
-- Please be careful to apply this function to arbitrary term, as this computation
-- may take very long time.
-- This is due to the ability to reorder each let/letrec definitions to match another
-- lambda term.
alphaEquiv ::
  (Eq a, Eq x) =>
  Lam v l a x -> Lam w f a x -> Bool
alphaEquiv x y = not . null $ unify emptySubst [(x,y)]

unify :: (Eq a, Eq x) =>
         Subst -> [(Lam v l a x, Lam w f a x)] -> [Subst]
unify facts [] = [facts]
unify facts ((Lam fx, Lam fy):equs) =
  case (fx, fy) of
    (App x1 x2, App y1 y2) -> unify facts ((x1,y1) : (x2,y2) : equs)
    (Abs _ x,   Abs _ y)   ->
      do facts' <- unify (openSubst facts) [(unScope x, unScope y)]
         unify (closeSubst facts') equs
    (Let xdefs xbody, Let ydefs ybody) ->
      do facts' <- unify (openSubst facts) [(unScope xbody, unScope ybody)]
         facts'' <- unifyLet facts' (map snd xdefs) (map snd ydefs)
         unify (closeSubst facts'') equs
    (LetRec xdefs xbody, LetRec ydefs ybody) ->
      do facts' <- unify (openSubst facts) [(unScope xbody, unScope ybody)]
         facts'' <- unifyLetRec facts' (map snd xdefs) (map snd ydefs)
         unify (closeSubst facts'') equs
    (FV x, FV y)             | x == y -> unify facts equs
    (BV dx i, BV dy j)       | dx == dy ->
        case lookupSubst dx i facts of
          Nothing -> unify (insertSubst dx i j facts) equs
          Just k  -> if j == k then unify facts equs else []
    (Lit x, Lit y) | x == y -> unify facts equs
    (Loc _ x, y) -> unify facts ((x, Lam y) : equs)
    (x, Loc _ y) -> unify facts ((Lam x, y) : equs)
    _ -> []

unifyLet ::
  (Eq a, Eq x) =>
  Subst -> [Lam v l a x] -> [Lam w f a x] -> [Subst]
unifyLet facts xs ys =
  do guard (length xs == length ys)
     let ixs = sortOn getHash $ zip3 [0..] (map lamHash xs) xs
         jys = sortOn getHash $ zip3 [0..] (map lamHash ys) ys
     guard (map getHash ixs == map getHash jys)
     let hashes = map getHash ixs
         dup = duplicity hashes
         facts' = shiftSubst (-1) facts
     facts'' <- go facts' (sortOn (dup . getHash) ixs) (sortOn (dup . getHash) jys)
     return (shiftSubst 1 facts'')
  where
    getHash (_,y,_) = y
    getIndex (i,_,_) = i

    go subst ixs jys =
      case findJust (\ix -> lookupSubst (-1) (getIndex ix) subst) ixs  of
        -- If there is known substitution (i => k), it must be xs[i] ~ ys[k]
        Just ((_i,hx,x), k, ixs') ->
          do ((_j,hy,y), jys') <- maybeToList $ find ((==k) . getIndex) jys
             guard (hx == hy)
             subst' <- unify subst [(x,y)]
             go subst' ixs' jys'
        -- If there is no known substitution, take one xs[i] and try unify to
        -- every possible ys[j]
        Nothing -> case ixs of
          []              -> [subst]
          (i,hx,x) : ixs' ->
            do ((j,hy,y), jys') <- pickup jys
               guard (hx == hy)
               subst' <- unify (insertSubst (-1) i j subst) [(x,y)]
               go subst' ixs' jys'

unifyLetRec ::
  (Eq a, Eq x) =>
  Subst -> [Scope (Lam v l a x)] -> [Scope (Lam w f a x)] -> [Subst]
unifyLetRec facts xs ys =
  do guard (length xs == length ys)
     let ixs = sortOn getHash $ zip3 [0..] (map (lamHash . unScope) xs) xs
         jys = sortOn getHash $ zip3 [0..] (map (lamHash . unScope) ys) ys
     guard (map getHash ixs == map getHash jys)
     let hashes = map getHash ixs
         dup = duplicity hashes
     go facts (sortOn (dup . getHash) ixs) jys
  where
    getHash (_,y,_) = y
    getIndex (i,_,_) = i

    -- Loop until there is no subst
    go subst ixs jys =
      case findJust (\ix -> lookupSubst 0 (getIndex ix) subst) ixs of
        -- If there is known substitution (i => k), it must be xs[i] ~ ys[k]
        Just ((_i,hx,x), k, ixs') ->
          do ((_j,hy,y), jys') <- maybeToList $ find ((==k) . getIndex) jys
             guard (hx == hy)
             subst' <- unify subst [(unScope x, unScope y)]
             go subst' ixs' jys'
        -- If there is no known substitution, take one xs[i] and try unify to
        -- every possible ys[j]
        Nothing -> case ixs of
          [] -> [subst]
          (i,hx,x) : ixs' ->
            do ((j,hy,y), jys') <- pickup jys
               guard (hx == hy)
               subst' <- unify (insertSubst 0 i j subst) [(unScope x, unScope y)]
               go subst' ixs' jys'

-- * Substitution

type Subst = Map Int (Map Int Int)

emptySubst :: Subst
emptySubst = Map.empty

lookupSubst :: Int -> Int -> Subst -> Maybe Int
lookupSubst d n subst =
  Map.lookup d subst >>= Map.lookup n

insertSubst :: Int -> Int -> Int -> Subst -> Subst
insertSubst d i j =
  Map.alter (Just . maybe (Map.singleton i j) (Map.insert i j)) d

shiftSubst :: Int -> Subst -> Subst
shiftSubst i = Map.mapKeysMonotonic (+ i)

openSubst, closeSubst :: Subst -> Subst
openSubst = Map.mapKeysMonotonic (\i -> if i >= 0 then i + 1 else i)
closeSubst = Map.mapKeysMonotonic (\i -> if i >= 1 then i - 1 else i) .
             Map.filterWithKey (\d _ -> d /= 0)

-- | Hash sans variable name and order of let(rec) binding.
lamHash :: Lam v l a x -> Word
lamHash = cata $ \case
  App e1 e2          -> e1 `combine` e2
  Abs _ (Scope body) -> 3 `combine` body
  Let defs (Scope body) -> hashSum $ 5 : body : sort (map snd defs)
  LetRec defs (Scope body) -> hashSum $ 7 : body : sort (map (unScope . snd) defs)
  FV _   -> 1
  BV d _ -> primes !! d
  Lit _    -> 1
  Loc _ e  -> e
  where
    primes :: [Word]
    primes = cycle [11,13,17,19,21,23,29,31,37,41,43,47,53,59,61]

    combine :: Word -> Word -> Word
    combine a = xor (16777619 * a)

    hashSum :: (Foldable f) => f Word -> Word
    hashSum = F.foldl' combine 0

-- * Utility

find :: (a -> Bool) -> [a] -> Maybe (a, [a])
find _ [] = Nothing
find p (a:as) =
  if p a
  then Just (a, as)
  else (fmap . fmap) (a:) (find p as)

findJust :: (a -> Maybe b) -> [a] -> Maybe (a, b, [a])
findJust _ [] = Nothing
findJust f (a:as) = case f a of
  Nothing -> fmap (\(a0,b0,as') -> (a0,b0,a:as')) (findJust f as)
  Just b  -> Just (a, b, as)

duplicity :: (Ord a) => [a] -> a -> Int
duplicity as = (duplicity' Map.!)
  where
    duplicity' = F.foldl' step Map.empty as
    step m a = Map.alter (Just . maybe 1 (+1)) a m

pickup :: [a] -> [(a,[a])]
pickup []     = []
pickup (a:as) = (a,as) : [ (b,a:bs) | (b,bs) <- pickup as ]
