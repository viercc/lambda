{-# LANGUAGE RankNTypes #-}
module Data.Lambda.ProofSearch (
  generate,

  module Control.Monad.NonDet
) where

import           Control.Monad

import           Control.Monad.Reader
import           Control.Monad.State

import qualified Data.Map.Strict      as Map
import           Data.Set             (Set)
import qualified Data.Set             as Set

import           Data.Lambda.Core     hiding (instantiate)
import           Data.TypeCheck.Core
import qualified Data.TypeCheck.Unify as Unify

import           Control.Monad.NonDet
import Data.List.Infinite (Infinite(..))
import qualified Data.List.Infinite as Inf

-- | Generate lambda expressions of given type.
generate ::
  Int ->
  [(VarName, TypeScheme)] ->
  TypeScheme ->
  NonDet (Lam VarName f a VarName)
generate maxCapacity env0 (TypeScheme vs t) =
  let bvs = Set.fromList vs
      tvs = uniqueNamesSans bvs
      fvs = uniqueNamesSans (Set.fromList (fst <$> env0))
      subst0 = emptySubst
      st = GenState subst0 bvs tvs fvs maxCapacity
  in nondet $ evalStateT (runReaderT (generate' t) env0) st

data GenState =
  GenState
    { getSubst    :: Subst
    , getRigids   :: Set TyVar
    , getTVSource :: Infinite TyVar
    , getFVSource :: Infinite VarName
    , getCapacity :: Int
    }

type Gen m = ReaderT [(VarName, TypeScheme)] (StateT GenState m)

generate' :: (MonadFail m, MonadPlus m)
          => Type -> Gen m (Lam VarName f a VarName)
generate' t =
  do s <- gets getSubst
     let t' = applySubst s t
     case t' of
       TC "->" [_,_] -> randomChoice [genExact t', genFun t']
       _             -> randomChoice [genApp t']

genExact :: (MonadFail m, MonadPlus m)
         => Type -> Gen m (Lam VarName f a VarName)
genExact t =
  do consumeCapacity 1
     s <- gets getSubst
     (x,u) <- ask >>= randomChoice . map return
     u' <- instantiate (applySubst s u)
     unify t u'
     return (fv_ x)

genFun :: (MonadFail m, MonadPlus m)
       => Type -> Gen m (Lam VarName l a VarName)
genFun t =
  do consumeCapacity 1
     [x,r] <- freshTVs 2
     [varX] <- freshFVs 1
     unify t (TV x ~> TV r)
     let le = (varX, TypeScheme [] (TV x))
     body <- local (le :) $ generate' (TV r)
     return (anyLoc $ makeAbs varX body)

genApp :: (MonadFail m, MonadPlus m)
       => Type -> Gen m (Lam VarName f a VarName)
genApp t =
  do s <- gets getSubst
     (f,u) <- ask >>= randomChoice . map return
     u' <- instantiate (applySubst s u)
     argTypes <- loop [] u'
     args <- traverse generate' argTypes
     return $ foldr (flip app_) (fv_ f) args
  where
    loop argTypes u = randomChoice [here, there]
      where
        here = unify t u >> return argTypes
        there =
          do consumeCapacity 1
             [x,r] <- freshTVs 2
             unify u (TV x ~> TV r)
             loop (TV x : argTypes) (TV r)

consumeCapacity :: MonadPlus m => Int -> Gen m ()
consumeCapacity n =
  do cap <- gets getCapacity
     let cap' = cap - n
     guard (cap' > 0)
     modify $ \st -> st{ getCapacity = cap' }

freshTVs :: Monad m => Int -> Gen m [TyVar]
freshTVs n =
  do tvs <- gets getTVSource
     let (vs, tvs') = Inf.splitAt n tvs
     modify $ \st -> st{ getTVSource = tvs' }
     return vs

freshFVs :: Monad m => Int -> Gen m [VarName]
freshFVs n =
  do fvs <- gets getFVSource
     let (vs, fvs') = Inf.splitAt n fvs
     modify $ \st -> st{ getFVSource = fvs' }
     return vs

instantiate :: Monad m => TypeScheme -> Gen m Type
instantiate (TypeScheme vs t) =
  do vs' <- freshTVs (length vs)
     let replacer = Subst $ Map.fromList (zip vs (map TV vs'))
     return (applySubst replacer t)

unify :: MonadPlus m => Type -> Type -> Gen m ()
unify t u =
  do rigids <- gets getRigids
     s0 <- gets getSubst
     let unifyResult = Unify.unifyWithRigids () rigids [(applySubst s0 t, applySubst s0 u)]
     case unifyResult of
       Left _   -> mzero
       Right s1 -> let s' = s1 @@ s0
                   in modify $ \st -> st{ getSubst = s' }
