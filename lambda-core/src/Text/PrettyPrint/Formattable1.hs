{-# LANGUAGE OverloadedStrings #-}
module Text.PrettyPrint.Formattable1(
  Formattable1(..),
  liftFormatPrec',
  formatPrec1, formatList1, formatDoc1
) where

import           Data.Functor.Const
import qualified Data.Map                     as Map
import qualified Data.Set                     as Set

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

-- | Formattable1 is Show1 in the sense Formattable is Show
class Formattable1 f where
    liftFormatPrec :: (Int -> x -> Doc a) -> ([x] -> Doc a) -> Int -> f x -> Doc a
    liftFormatList :: (Int -> x -> Doc a) -> ([x] -> Doc a) -> [f x] -> Doc a
    liftFormatList fprec flist =
      defaultFormatList (liftFormatPrec fprec flist)

liftFormatPrec' :: (Formattable1 f) => (Int -> x -> Doc a) -> Int -> f x -> Doc a
liftFormatPrec' fprec = liftFormatPrec fprec (defaultFormatList fprec)

formatPrec1 :: (Formattable x, Formattable1 f) => Int -> f x -> Doc a
formatPrec1 = liftFormatPrec formatPrec formatList

formatList1 :: (Formattable x, Formattable1 f) => [f x] -> Doc a
formatList1 = liftFormatList formatPrec formatList

formatDoc1 :: (Formattable x, Formattable1 f) => f x -> Doc a
formatDoc1 = formatPrec1 0

instance (Formattable a) => Formattable1 (Const a) where
  liftFormatPrec _ _ p (Const a) = formatCApp p "Const" a

instance (Formattable a) => Formattable1 ((,) a) where
  liftFormatPrec fprec _ _ (a,b) = tupled [formatDoc a, fprec 0 b]

instance (Formattable a, Formattable b) =>
         Formattable1 ((,,) a b) where
  liftFormatPrec fprec _ _ (a,b,c) =
    tupled [formatDoc a, formatDoc b, fprec 0 c]

instance Formattable1 Maybe where
  liftFormatPrec _ _ _ Nothing = "Nothing"
  liftFormatPrec fprec _ p (Just a) = formatCAppBy fprec p "Just" a

instance (Formattable a) => Formattable1 (Either a) where
  liftFormatPrec _ _ p (Left a)      = formatCApp p "Left" a
  liftFormatPrec fprec _ p (Right b) = formatCAppBy fprec p "Right" b

instance Formattable1 [] where
  liftFormatPrec _ flist _ = flist

instance (Formattable k) => Formattable1 (Map.Map k) where
  liftFormatPrec fprec _ p x =
    let doc = formatAssocListBy fprec (Map.toList x)
    in formatCApp p "Map.fromList" doc

instance Formattable1 Set.Set where
  liftFormatPrec _ flist p x =
    let doc = flist (Set.toList x)
    in formatCApp p "Set.fromList" doc

formatAssocListBy :: (Formattable k) => (Int -> v -> Doc a) -> [(k,v)] -> Doc a
formatAssocListBy fprec =
  formatListBy (\(k,v) -> tupled [formatDoc k, fprec 0 v])
