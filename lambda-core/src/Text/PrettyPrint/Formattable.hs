{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
module Text.PrettyPrint.Formattable(
  Formattable(..),
  formatDoc,

  customRender,
  format, pprint,
  defaultFormatList,
  formatApp, formatCApp
) where

import           Data.Void

import           Prettyprinter
import           Prettyprinter.Render.String
import           Text.PrettyPrint.Util

import           Data.Functor.Const
import qualified Data.Map            as Map
import qualified Data.Set            as Set

-- | Formattable is a Show-like alternative to Pretty.
class Formattable x where
    formatPrec :: Int -> x -> Doc a
    default formatPrec :: Show x => Int -> x -> Doc a
    formatPrec p x = pretty (showsPrec p x "")

    formatList :: [x] -> Doc a
    formatList = defaultFormatList formatPrec

formatDoc :: (Formattable x) => x -> Doc a
formatDoc = formatPrec 0

customRender :: Doc a -> SimpleDocStream a
customRender = layoutSmart defaultLayoutOptions

format :: Formattable x => x -> String
format = ($ []) . renderShowS . customRender . formatDoc

pprint :: Formattable x => x -> IO ()
pprint = putStr . format

instance Formattable () where
instance Formattable Bool where
instance Formattable Char where
  formatList = prettyList
instance Formattable Float where
instance Formattable Double where
instance Formattable Int where
instance Formattable Integer where

instance Formattable Void where
  formatPrec _ = absurd

instance Formattable (Doc a) where
  formatPrec _ = unAnnotate

instance (Formattable a) => Formattable (Const a x) where
  formatPrec p (Const a) = formatCApp p "Const" a

instance (Formattable a, Formattable b) => Formattable (a,b) where
  formatPrec _ (a,b) = tupled [formatDoc a, formatDoc b]

instance (Formattable a, Formattable b, Formattable c) =>
         Formattable (a,b,c) where
  formatPrec _ (a,b,c) = tupled [formatDoc a, formatDoc b, formatDoc c]

instance (Formattable a) => Formattable (Maybe a) where
  formatPrec _ Nothing  = "Nothing"
  formatPrec p (Just a) = formatCApp p "Just" a

instance (Formattable a, Formattable b) => Formattable (Either a b) where
  formatPrec p (Left a)  = formatCApp p "Left" a
  formatPrec p (Right b) = formatCApp p "Right" b

instance (Formattable a) => Formattable [a] where
  formatPrec _ = formatList

instance (Formattable k, Formattable a) => Formattable (Map.Map k a) where
  formatPrec p x = formatCApp p "Map.fromList" (Map.toList x)

instance (Formattable a) => Formattable (Set.Set a) where
  formatPrec p x = formatCApp p "Set.fromList" (Set.toList x)

defaultFormatList :: (Int -> a -> Doc x) -> [a] -> Doc x
defaultFormatList f = formatListBy (f 0)

formatApp :: (Formattable a, Formattable b) => Int -> a -> b -> Doc x
formatApp = formatAppBy formatPrec formatPrec

formatCApp :: (Formattable b) => Int -> Doc x -> b -> Doc x
formatCApp = formatCAppBy formatPrec
