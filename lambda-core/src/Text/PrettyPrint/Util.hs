module Text.PrettyPrint.Util(
  formatListBy, formatAppBy, formatCAppBy,
  parensIf,
  module Prettyprinter
) where

import           Prettyprinter

formatListBy :: (a -> Doc x) -> [a] -> Doc x
formatListBy f as = list $ fmap f as

formatAppBy :: (Int -> a -> Doc x) -> (Int -> b -> Doc x) -> Int -> a -> b -> Doc x
formatAppBy fpa fpb p x y =
  parensIf (p > 10) $ fillSep [fpa 10 x, fpb 11 y]

formatCAppBy :: (Int -> b -> Doc x) -> Int -> Doc x -> b -> Doc x
formatCAppBy = formatAppBy (\_ a -> a)

parensIf :: Bool -> Doc x -> Doc x
parensIf b = if b then parens else id
