{-# LANGUAGE DeriveLift        #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.TypeCheck.Core(
  -- * Basic type representation
  Type(..),
  TyVar,
  (~>),
  substTyVar,
  -- * Substitution on type
  Subst(..), emptySubst, (+->), (@@),
  -- * Type scheme
  TypeScheme(..),
  renameTyVars,
  normalizeTypeScheme,
  equivTypeScheme,
  -- * Type environment (assignment of types to variables)
  TypeEnv(..), envEmpty, envDelete, envInsert, envLookup, generalize,
  -- * The class for operations against a type or types
  FType(..)
) where

import qualified Data.Foldable                as F
import           Data.Function                (on)
import qualified Data.Map.Strict              as Map
import           Data.Maybe                   (fromMaybe)
import           Data.Set                     ((\\))
import qualified Data.Set                     as Set

import           Data.VarName
import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Language.Haskell.TH.Syntax   (Lift (..))
import qualified Data.List.Infinite as Inf

-- | Representation of Type
data Type = TV TyVar | TC String [Type]
          deriving (Show, Eq, Ord, Lift)

-- | Arrow type
(~>) :: Type -> Type -> Type
a ~> b = TC "->" [a, b]
infixr 8 ~>

-- | Type variable
type TyVar = VarName

-- | Substitution
newtype Subst = Subst { runSubst :: Map.Map TyVar Type }
  deriving (Show, Eq)

-- | empty substitution; equivalent to id
emptySubst :: Subst
emptySubst = Subst Map.empty

-- | Substitution for single variable
(+->) :: TyVar -> Type -> Subst
x +-> t = Subst $ Map.singleton x t
infix 5 +->

-- | Compose two substitution
-- > applySubst (s1 @@ s2) = applySubst s1 . applySubst s2
(@@) :: Subst -> Subst -> Subst
s1 @@ s2 =
  let s2' = Map.map (applySubst s1) (runSubst s2)
  in Subst $ Map.union (runSubst s1) s2'
infixl 3 @@

-- | Type scheme
data TypeScheme = TypeScheme [TyVar] Type
          deriving (Show, Eq, Ord, Lift)

renameTyVars :: Set.Set TyVar -> TypeScheme -> TypeScheme
renameTyVars usedVars tscheme@(TypeScheme vs t) =
  let usedNames = usedVars `Set.union` freeVars tscheme
      freshNames = Inf.filter (`Set.notMember` usedNames) uniqueNames
      varMap = Map.fromList (zip vs (Inf.toList freshNames))
  in TypeScheme (map (varMap Map.!) vs)
                (substTyVar varMap t)

normalizeTyVarOrder :: TypeScheme -> TypeScheme
normalizeTyVarOrder (TypeScheme vs t) =
    TypeScheme (go (Set.fromList vs) [t]) t
  where
    go vSet [] = Set.toList vSet
    go vSet (TV tv : rest) =
      if Set.member tv vSet
      then tv : go (Set.delete tv vSet) rest
      else go vSet rest
    go vSet (TC _ args : rest) = go vSet (args ++ rest)

normalizeTypeScheme :: TypeScheme -> TypeScheme
normalizeTypeScheme t = renameTyVars (freeVars t) $ normalizeTyVarOrder t

equivTypeScheme :: TypeScheme -> TypeScheme -> Bool
equivTypeScheme = (==) `on` normalizeTypeScheme

-- | Type environment
newtype TypeEnv v = TypeEnv (Map.Map v TypeScheme)
                  deriving (Show, Eq, Ord)

instance FType (TypeEnv v) where
  freeVars (TypeEnv env) = F.foldMap freeVars env
  applySubst s (TypeEnv env) = TypeEnv $ Map.map (applySubst s) env

envEmpty :: TypeEnv v
envEmpty = TypeEnv Map.empty

envDelete :: (Ord v) => v -> TypeEnv v -> TypeEnv v
envDelete x (TypeEnv env) = TypeEnv $ Map.delete x env

envInsert :: (Ord v) => v -> TypeScheme -> TypeEnv v -> TypeEnv v
envInsert x t (TypeEnv env) = TypeEnv $ Map.insert x t env

envLookup :: (Ord v) => v -> TypeEnv v -> Maybe TypeScheme
envLookup x (TypeEnv env) = Map.lookup x env

generalize :: Type -> TypeEnv v -> TypeScheme
generalize t env =
  TypeScheme (Set.toList (freeVars t \\ freeVars env)) t

-- | Class for collection of Types
class FType a where
  freeVars :: a -> Set.Set TyVar
  applySubst :: Subst -> a -> a

instance FType Type where
  freeVars (TV v)      = Set.singleton v
  freeVars (TC _ args) = Set.unions (map freeVars args)

  applySubst s = go where
    go t@(TV x)     = fromMaybe t $ Map.lookup x (runSubst s)
    go (TC tc args) = TC tc (map go args)

instance FType TypeScheme where
  freeVars (TypeScheme vs t) =
    freeVars t \\ Set.fromList vs

  applySubst s (TypeScheme vs t) =
    let s' = Subst $ foldr Map.delete (runSubst s) vs
        s't = applySubst s' t
    in TypeScheme vs s't

substTyVar :: (FType ts) => Map.Map TyVar TyVar -> ts -> ts
substTyVar rep = applySubst s where
  s = Subst (Map.map TV rep)

{- pretty prints -}
instance Formattable Type where
  formatPrec prec t = case t of
    TV v -> formatDoc v
    TC c [] -> pretty c
    TC "->" [t1, t2] ->
      let d1 = formatPrec 2 t1
          d2 = formatPrec 1 t2
      in parensIf (prec > 1) $ d1 <+> "->" <+> d2
    TC c ts -> parensIf (prec > 10) $
                pretty c <+> hsep (map (formatPrec 11) ts)

instance Formattable TypeScheme where
  formatPrec _ (TypeScheme vars t) =
      quantifiers <+> formatDoc t
    where
      quantifiers =
        if null vars
        then mempty
        else let varsDoc = map formatDoc vars
             in "forall" <+> hsep varsDoc <> dot
