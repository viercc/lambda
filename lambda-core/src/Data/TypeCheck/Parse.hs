{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Data.TypeCheck.Parse(
  typeP, typeSchemeP, parseType, parseTypeScheme
) where

import           Data.Lambda.Parse.Util
import           Data.VarName
import           Text.Megaparsec

import           Data.TypeCheck.Core

parseType :: String -> Either String Type
parseType = either (Left . show) Right . parse typeP ""

parseTypeScheme :: String -> Either String TypeScheme
parseTypeScheme = either (Left . show) Right . parse typeSchemeP ""

variableP :: Parser TyVar
variableP = VarName <$> makeIdentifierP ["forall"]

typeP :: Parser Type
typeP = space' *> arrP where
  tvP :: Parser Type
  tvP = TV <$> variableP

  prec11 :: Parser Type
  prec11 = tvP <|> parens' arrP <|> tc0P
  
  tc0P :: Parser Type
  tc0P = TC <$> makeCIdentifierP [] <*> pure []
  
  tcAppP :: Parser Type
  tcAppP = TC <$> makeCIdentifierP [] <*> some prec11
  
  prec10 :: Parser Type
  prec10 = try tcAppP <|> prec11
  
  arrP :: Parser Type
  arrP = foldr1 (~>) <$> sepBy1 prec10 (symbol' "->")

typeSchemeP :: Parser TypeScheme
typeSchemeP = space' *> (TypeScheme <$> varlistP <*> typeP) where
  varlistP = option [] (try $ symbol' "forall" *>
                              some variableP <*
                              symbol' ".")
