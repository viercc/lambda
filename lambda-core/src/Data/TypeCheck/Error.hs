{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.TypeCheck.Error(
  TypeError(..)
) where

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Data.TypeCheck.Core

-- | Error reporting
data TypeError p v =
    UnifyFailOccurCheck p TyVar Type
  | UnifyFailMisMatch p Type Type
  | ErrorUndefinedVar p v Type
  | ErrorAnnotation p TypeScheme TypeScheme
  | OtherError p String
  deriving (Show, Eq, Ord, Functor)

instance FType (TypeError p v) where
  freeVars = error "You need free vars of type error, really?"
  applySubst s mes =
    let f :: FType t => t -> t
        f = applySubst s
    in case mes of
         UnifyFailOccurCheck m v t -> UnifyFailOccurCheck m v (f t)
         UnifyFailMisMatch m t u   -> UnifyFailMisMatch m (f t) (f u)
         ErrorUndefinedVar m x t   -> ErrorUndefinedVar m x (f t)
         ErrorAnnotation m t u     -> ErrorAnnotation m (f t) (f u)
         _                         -> mes

instance (Pretty p, Formattable v) => Pretty (TypeError p v) where
  pretty = pp where
    pp (UnifyFailOccurCheck p v t) =
      pretty p <#>
      "Can't unify (recursive):" <+>
        formatDoc v <+> "~" <+> formatDoc t
    pp (UnifyFailMisMatch p t u) =
      pretty p <#>
      "Can't Unify (mismatch):" <+>
        formatDoc t <+> "~" <+> formatDoc u
    pp (ErrorUndefinedVar p x t) =
      pretty p <#>
      "Found Undefined Variable:" <+>
        formatDoc x <+> "::" <+> formatDoc t
    pp (ErrorAnnotation p t u) =
      pretty p <#>
      "Type does not match to expected:" <#>
      indent 4 (
          "Actual:   " <> formatDoc t <#>
          "Expected: " <> formatDoc u
      )
    pp (OtherError p s) =
      pretty p <#>
      pretty s

    a <#> b = vsep [a,b]
