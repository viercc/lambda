{-# LANGUAGE FlexibleContexts #-}
module Data.TypeCheck.Unify(
  unify, unifyWithRigids, isGeneralThan
) where

import           Data.Set (Set)
import qualified Data.Set as Set

import           Data.TypeCheck.Core
import           Data.TypeCheck.Error

-- | Unification
unify :: p -> [(Type,Type)] -> Either (TypeError p v) Subst
unify pos equsStart = solve equsStart emptySubst
  where
    solve [] subst = return subst
    solve ((t1,t2):equs') subst =
      case (applySubst subst t1, applySubst subst t2) of
        (TV v1, TV v2) ->
          let subst' | v1 == v2 = subst
                     | otherwise = v1 +-> TV v2 @@ subst
          in solve equs' subst'
        (TV v1, t2') ->
          if v1 `occursIn` t2'
          then Left $ UnifyFailOccurCheck pos v1 t2'
          else let subst' = v1 +-> t2' @@ subst
               in solve equs' subst'
        (t1', TV v2) ->
          if v2 `occursIn` t1'
          then Left $ UnifyFailOccurCheck pos v2 t1'
          else let subst' = v2 +-> t1' @@ subst
               in solve equs' subst'
        (t1'@(TC c1 ts1), t2'@(TC c2 ts2)) ->
          if c1 == c2 && length ts1 == length ts2
          then solve (zip ts1 ts2 ++ equs') subst
          else Left $ UnifyFailMisMatch pos t1' t2'

unifyWithRigids :: p -> Set TyVar -> [(Type,Type)] -> Either (TypeError p v) Subst
unifyWithRigids pos rigids equsStart = solve equsStart emptySubst
  where
    solve [] subst = return subst
    solve ((t1,t2):equs') subst =
      case (applySubst subst t1, applySubst subst t2) of
        (TV v1, TV v2)
          | v1 `Set.notMember` rigids &&
            v2 `Set.notMember` rigids ->
          let subst' | v1 == v2 = subst
                     | otherwise = v1 +-> TV v2 @@ subst
          in solve equs' subst'
        (TV v1, t2') | v1 `Set.notMember` rigids ->
          if v1 `occursIn` t2'
          then Left $ UnifyFailOccurCheck pos v1 t2'
          else let subst' = v1 +-> t2' @@ subst
               in solve equs' subst'
        (t1', TV v2) | v2 `Set.notMember` rigids ->
          if v2 `occursIn` t1'
          then Left $ UnifyFailOccurCheck pos v2 t1'
          else let subst' = v2 +-> t1' @@ subst
               in solve equs' subst'
        (t1'@(TC c1 ts1), t2'@(TC c2 ts2)) ->
          if c1 == c2 && length ts1 == length ts2
          then solve (zip ts1 ts2 ++ equs') subst
          else Left $ UnifyFailMisMatch pos t1' t2'
        (t1'@(TV v1), t2'@(TV v2)) ->
          -- Only reach here if both v1 and v2 is rigid
          if v1 == v2
          then solve equs' subst
          else Left $ UnifyFailMisMatch pos t1' t2'
        (t1', t2') -> Left $ UnifyFailMisMatch pos t1' t2'

occursIn :: TyVar -> Type -> Bool
occursIn v1 (TV v2)    = v1 == v2
occursIn v1 (TC _ ts2) = any (occursIn v1) ts2

isGeneralThan :: p -> TypeScheme -> TypeScheme -> Either (TypeError p v) Subst
isGeneralThan pos (TypeScheme tvs t0) us =
    solve [(t0,u0)] emptySubst
  where
    TypeScheme _ u0 = renameTyVars (freeVars t0) us
    tvsSet = Set.fromList tvs
    
    split ty = case ty of
      TV v | Set.member v tvsSet -> Right v
      _                          -> Left ty
    
    solve [] s = return s
    solve ((t,u):equs) s =
      case (split (applySubst s t), u) of
        (Right tv, _) ->
          if not (tv `occursIn` u)
            then solve equs (tv +-> u @@ s)
            else Left $ UnifyFailOccurCheck pos tv u
        (Left (TV tv), TV uv)
          | tv == uv -> solve equs s
        (Left (TC tc targs), TC uc uargs)
          | tc == uc -> solve (zip targs uargs ++ equs) s
        (Left t', _) -> Left $ UnifyFailMisMatch pos t' u
