{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
module Data.TypeCheck.TypeInfer(
  -- * Type inference
  TypeInfer(),
  foundError, fatal,
  freshTyVar, freshTyVarFor,
  getEnv, localEnv,
  getPos, withPos,
  instantiate,
  inferVar, undefinedVar, inferFuncApp, inferFuncAbs,

  Declaration(..), Definition,
  inferLet, inferLets, inferLetDefs,
  inferLetRec, inferLetRecs, inferLetRecDefs,
  inferTypeAnnot,

  -- * Running TypeCheck monad
  TypeCheckResult(..),
  runTypeInfer
) where

import           Control.Arrow

import           Data.Foldable (foldrM)
import           Control.Monad.Except
import           Control.Monad.RWS

import qualified Data.Map.Strict         as Map

import           Data.TypeCheck.Core
import           Data.TypeCheck.Error
import           Data.TypeCheck.Unify
import           Data.VarName

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util
import Data.Traversable (forM)

-- | Type Inference
newtype TypeInfer p v a = TypeInfer {
    _runTypeInfer :: RWST (p, TypeEnv v)
                          [TypeError p v]
                          (NameGenerator TyVar)
                          (Either (TypeError p v)) a
  } deriving (Functor, Applicative, Monad)

foundError :: TypeError p v -> TypeInfer p v ()
foundError e = TypeInfer $ tell [e]

fatal :: TypeError p v -> TypeInfer p v a
fatal e = TypeInfer $ throwError e

freshTyVar :: TypeInfer p v TyVar
freshTyVar = TypeInfer $ state requestName

freshTyVarFor :: TyVar -> TypeInfer p v TyVar
freshTyVarFor x = TypeInfer $ state (requestNameFor x)

holeType :: TypeInfer p v Type
holeType = TV <$> freshTyVarFor (VarName "?")

getPos :: TypeInfer p v p
getPos = TypeInfer $ asks fst

withPos :: p -> TypeInfer p v a -> TypeInfer p v a
withPos p (TypeInfer m) = TypeInfer $ local (first (const p)) m

getEnv :: TypeInfer p v (TypeEnv v)
getEnv = TypeInfer $ asks snd

localEnv :: (TypeEnv v -> TypeEnv v) -> TypeInfer p v a -> TypeInfer p v a
localEnv f (TypeInfer m) = TypeInfer $ local (second f) m

instantiate :: TypeScheme -> TypeInfer p v ([TyVar], Type)
instantiate (TypeScheme xs t) =
  do xs' <- mapM freshTyVarFor xs
     let rep = Map.fromList (zip xs xs')
     return (xs', substTyVar rep t)

inferVar :: (Ord v) => v -> TypeInfer p v (Type, Subst)
inferVar x =
  do env <- getEnv
     case envLookup x env of
       Nothing -> undefinedVar x
       Just tsch -> do (_, t) <- instantiate tsch
                       return (t, emptySubst)

undefinedVar :: v -> TypeInfer p v (Type, Subst)
undefinedVar x =
  do pos <- getPos
     t <- holeType
     foundError $ ErrorUndefinedVar pos x t
     return (t, emptySubst)

inferFuncApp :: TypeInfer p v (Type, Subst) ->
                TypeInfer p v (Type, Subst) ->
                TypeInfer p v (Type, Subst)
inferFuncApp e1 e2 =
  do (t1, s1) <- e1
     (t2, s2) <- localEnv (applySubst s1) e2
     b <- freshTyVar
     pos <- getPos
     v <- case unify pos [(applySubst s2 t1, t2 ~> TV b)] of
            Right v -> return v
            Left e -> do foundError e
                         t <- holeType
                         return (b +-> t)
     let s' = v @@ s2 @@ s1
     return (applySubst v (TV b), s')

inferFuncAbs :: (Ord v) => v -> TypeInfer p v (Type, Subst)
                             -> TypeInfer p v (Type, Subst)
inferFuncAbs x body =
  do tx <- freshTyVar
     (tbody, sbody) <- localEnv (envInsert x (TypeScheme [] (TV tx))) body
     return (applySubst sbody (TV tx) ~> tbody, sbody)

data Declaration v = NoAnnot v | Annot v TypeScheme
                   deriving (Show, Eq, Ord)

type Definition p v = (Declaration v, TypeInfer p v (Type,Subst))

inferLet ::
  (Ord v) => Definition p v ->
             TypeInfer p v (Type,Subst) ->
             TypeInfer p v (Type,Subst)
inferLet def = inferLets [def]

inferLets ::
  (Ord v) => [Definition p v] ->
             TypeInfer p v (Type,Subst) ->
             TypeInfer p v (Type,Subst)
inferLets defs body =
  do (env', s1) <- inferLetDefs defs
     (t2, s2) <- localEnv (const env') body
     return (t2, s2 @@ s1)

inferLetDefs ::
  (Ord v) => [Definition p v] -> TypeInfer p v (TypeEnv v, Subst)
inferLetDefs defs =
  do env <- getEnv
     let state0 = ([], emptySubst, env)
     (types, s, env') <- foldrM inferDef state0 defs
     tschemes <- generalizeAll env' s types
     let env'' = foldr (\(x,t) e -> envInsert x t e) env' tschemes
     return (env'', s)
  where
    inferDef (dec, e) (types, s0, env) =
      do (t, s1) <- localEnv (const env) e
         let env' = applySubst s1 env
         return ((dec,t):types, s1 @@ s0, env')
    
    generalizeAll env s = mapM (generalizeOne env s)
    
    generalizeOne env s (NoAnnot x, t) =
      let tscheme = generalize (applySubst s t) env
      in return (x, tscheme)
    generalizeOne _   s (Annot x tscheme, t) =
      do _ <- inferTypeAnnot (return (t,s)) tscheme
         return (x, tscheme)
    
inferLetRec ::
  (Ord v) => Definition p v ->
             TypeInfer p v (Type,Subst) ->
             TypeInfer p v (Type,Subst)
inferLetRec def = inferLetRecs [def]

inferLetRecs ::
  (Ord v) => [Definition p v] ->
             TypeInfer p v (Type,Subst) ->
             TypeInfer p v (Type,Subst)
inferLetRecs defs body =
  do (env', s1) <- inferLetRecDefs defs
     (t2, s2) <- localEnv (const env') body
     return (t2, s2 @@ s1)

inferLetRecDefs ::
  (Ord v) => [Definition p v] ->
             TypeInfer p v (TypeEnv v,Subst)
inferLetRecDefs defs =
  do env <- getEnv
     env' <- makeEnv (map fst defs) env
     (env'', s) <- checkAll defs env' emptySubst
     let env''' = generalizeAll (map fst defs) (applySubst s env) env''
     return (env''', s)
  where
    makeEnv decs env =
      do envEntries <- forM decs $ \case
           NoAnnot x ->
             do xv <- freshTyVar
                return (x, TypeScheme [] (TV xv))
           Annot x scheme ->
                return (x, scheme)
         return (foldr (uncurry envInsert) env envEntries)
    
    checkAll ((NoAnnot x,e):rest) env s0 =
      do pos <- getPos
         us <- case envLookup x env of
            Nothing -> fatal $ OtherError pos "Implementation Bug!"
            Just us -> pure us
         (_, u) <- instantiate us
         (t, s1) <- localEnv (const env) e
         (_, s2) <- case unify pos [(t,applySubst s1 u)] of
           Left err    -> foundError err >> return (t, s1)
           Right subst -> return (applySubst subst t, subst @@ s1)
         let env' = applySubst s2 env
         checkAll rest env' (s2 @@ s0)
    checkAll ((Annot _x scheme,e):rest) env s0 =
      do (_, s1) <- localEnv (const env) $ inferTypeAnnot e scheme
         let env' = applySubst s1 env
         checkAll rest env' (s1 @@ s0)
    checkAll [] env s0 = return (env, s0)
    
    generalizeAll decs envOuter env = foldr (generalizeStep envOuter) env decs
    generalizeStep envOuter dec env =
      case dec of
        NoAnnot x ->
          case envLookup x env of
            Nothing -> env
            Just (TypeScheme _ t) -> envInsert x (generalize t envOuter) env
        Annot _ _ -> env

inferTypeAnnot :: TypeInfer p v (Type, Subst) -> TypeScheme ->
                  TypeInfer p v (TypeScheme, Subst)
inferTypeAnnot e scheme =
  do (t1, s1) <- e
     env <- applySubst s1 <$> getEnv
     pos <- getPos
     let tscheme = generalize t1 env
     case isGeneralThan pos tscheme scheme of
       Right _ -> return (tscheme, s1)
       Left _ -> do foundError $ ErrorAnnotation pos tscheme scheme
                    return (tscheme, s1)

-- | Type Checking
data TypeCheckResult p v a =
       Good a | Bad [TypeError p v]
       deriving (Show, Eq, Ord, Functor)

runTypeInfer :: p -> TypeInfer p v a -> TypeCheckResult p v a
runTypeInfer pos (TypeInfer m) =
  case runRWST m (pos, TypeEnv Map.empty) (newNameGenerator (const True)) of
    Left e         -> Bad [e]
    Right (a,_,es) -> if null es then Good a else Bad es

instance (Pretty p, Formattable v, Formattable a) =>
         Formattable (TypeCheckResult p v a) where
  formatPrec n (Good a) = formatPrec n a
  formatPrec _ (Bad es) =
    vsep [ pretty "Type check failed!"
         , indent 4 (vsep (map pretty es)) ]
         <> hardline
