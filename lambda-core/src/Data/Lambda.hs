module Data.Lambda (
  -- * Depending Module
  module Data.Void,
  module Data.Functor.Void1,
  Base, Recursive(..), Corecursive(..),
  
  -- * Core types
  Lam(..),
  LamF(..),
  Scope(..),

  -- * Variable names
  IsName(..),
  
  -- * Pure lambda calculus
  pureToAny, anyLiteral, anyLoc,
  Lam',
  
  -- * Construction
  app_, abs_, let_, letrec_, bv_, fv_, lit_, loc_,
  (|$|),
  makeFV, makeAbs, makeLet, makeLetRec,

  -- * Formatting lambda expression
  formatLam
) where

import Data.Void
import Data.Functor.Void1
import Data.Functor.Foldable

import Data.VarName ()

import Data.Lambda.Core
import Data.Lambda.Format