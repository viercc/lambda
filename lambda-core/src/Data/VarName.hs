{-# LANGUAGE DeriveLift   #-}
{-# LANGUAGE RankNTypes   #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE PostfixOperators #-}
module Data.VarName (
  VarName(..),
  IsName(..),

  freshVar,
  freshVars,
  uniqueNamesSans,

  NameGenerator(),
  newNameGenerator,
  requestName,
  requestNameFor
) where

import           Data.String

import           Data.Char                    (isAlpha)
import qualified Data.Map.Strict              as Map
import           Data.Maybe                   (fromMaybe)
import qualified Data.Set                     as Set
import Data.Set (Set)

import           Language.Haskell.TH.Syntax   (Lift)
import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import Data.List.Infinite (Infinite (..), (...))
import qualified Data.List.Infinite as Inf
import Data.List.NonEmpty (NonEmpty (..))

-- | Class of variable names
class (Ord v, IsString v) => IsName v where
  derive :: v -> Infinite v
  uniqueNames :: Infinite v

---- base types
newtype VarName = VarName String
                deriving (Show, Read, Eq, Ord, Lift)

instance Formattable VarName where
  formatPrec _ (VarName x) = pretty x

instance IsString VarName where
  fromString = VarName

instance IsName VarName where
  derive (VarName x) =
    let x0 = takeWhile isAlpha x
    in fmap (\p -> VarName (x0 ++ p)) postfixes
  uniqueNames = Inf.concatMap (\p -> fmap (\a -> VarName (a : p)) alphabets) postfixes

-- English alphabet sans i, l, o
alphabets :: NonEmpty Char
alphabets = 'a' :| "bcdefghjkmnpqrstuvwxyz"

postfixes :: Infinite String
postfixes = [] :< (show <$> ((1 :: Integer) ...))

freshVar :: (IsName v) => v -> Set.Set v -> v
freshVar x used = newVar (x :< derive x)
  where
    newVar (v :< vs)
      | v `Set.member` used = newVar vs
      | otherwise           = v

freshVars :: (IsName v) => [v] -> Set.Set v -> ([v], Set.Set v)
freshVars xs used0 =
  let f x (ys, used) =
        let y = freshVar x used
        in (y:ys, Set.insert y used)
  in foldr f ([], used0) xs

uniqueNamesSans :: IsName v => Set v -> Infinite v
uniqueNamesSans = go uniqueNames
  where
    go (v :< vs) banSet
      | Set.null banSet       = v :< vs
      | v `Set.member` banSet = go vs (Set.delete v banSet)
      | otherwise             = v :< go vs banSet

data NameGenerator v = NameGenerator {
    _ngSrc       :: Infinite v
  , _ngDeriveSrc :: Map.Map v (Infinite v)
  , _ngFilter    :: v -> Bool
  }

newNameGenerator :: IsName v => (v -> Bool) -> NameGenerator v
newNameGenerator = NameGenerator uniqueNames Map.empty

requestName :: IsName v => NameGenerator v -> (v, NameGenerator v)
requestName ng =
  let (x :< src') = _ngSrc ng
      (v, ng') = requestNameFor x ng
  in (v, ng'{ _ngSrc = src' })

requestNameFor :: IsName v => v -> NameGenerator v -> (v, NameGenerator v)
requestNameFor x ng =
  let x0 = Inf.head (derive x)
      dsrc = _ngDeriveSrc ng
      vs = fromMaybe (derive x) $ Map.lookup x0 dsrc
      (v,vs') = takeFirst (_ngFilter ng) vs
      dsrc' = Map.insert x0 vs' dsrc
  in (v, ng{ _ngDeriveSrc = dsrc' })

takeFirst :: (v -> Bool) -> Infinite v -> (v, Infinite v)
takeFirst predicate xs =
  case Inf.dropWhile (not . predicate) xs of
    x :< xs' -> (x, xs')
