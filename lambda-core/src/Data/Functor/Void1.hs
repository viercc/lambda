module Data.Functor.Void1 where

import Data.Functor.Const
import Data.Void

type Void1 = Const Void

absurd1 :: Void1 a -> b
absurd1 = absurd . getConst
