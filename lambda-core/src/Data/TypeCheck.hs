module Data.TypeCheck(
  -- * Basic type representation
  Type(..), TyVar,
  (~>),
  -- * Substitution on type
  Subst(..), emptySubst, (+->), (@@),
  -- * Type scheme
  TypeScheme(..),
  renameTyVars,
  -- * Type environment (assignment of types to variables)
  TypeEnv(..), envEmpty, envDelete, envInsert, generalize,
  -- * The class for operations against a type or types
  FType(..),

  -- * Error reporting
  module Data.TypeCheck.Error,
  
  -- * Type inference
  TypeInfer(),
  foundError, fatal,
  instantiate,
  inferVar, inferFuncApp, inferFuncAbs,
  inferLet,
  inferTypeAnnot,
  
  -- * Running TypeCheck monad
  TypeCheckResult(..),
  runTypeInfer
) where

import Data.TypeCheck.Core
import Data.TypeCheck.Error
import Data.TypeCheck.TypeInfer

