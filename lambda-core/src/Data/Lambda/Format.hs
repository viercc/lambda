{-# LANGUAGE OverloadedStrings #-}
module Data.Lambda.Format(
   formatLam,
   formatLamP
) where

import Data.Lambda.Core

import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util
import Prettyprinter.Render.String (renderString)

-----------------------------------------------------------
---- serialize to text

parensIf' :: Bool -> Doc a -> Doc a
parensIf' cond = if cond then parens' else id

parens' :: Doc a -> Doc a
parens' doc = group $ nest 1 $ vcat [pretty '(' <> doc, pretty ')']

(!?) :: [a] -> Int -> Maybe a
as !? n
  | n < 0 = Nothing
  | otherwise =
      case drop n as of
        []    -> Nothing
        (a:_) -> Just a

formatLamP :: (Formattable v,
               Formattable a,
               Formattable x) =>
              Int -> Lam v l a x -> Doc ann
formatLamP prec e = group (go [] [] prec e) <> hardline
  where
    makeLam p env []   body = body p env
    makeLam p env args body =
      let env' = map (:[]) args ++ env
          argDocs = map formatDoc $ reverse args
          bodyDoc = body 2 env'
          doc = group $ nest 3 $ "\\" <> hsep argDocs <+> "->"
                  <#> group bodyDoc
      in parensIf' (p > 2) doc
    block tag contents =
      let doc = vsep $ punctuate semi contents
      in nest 2 (tag <+> "{" <#> doc) <#> "}"
    lookupEnv env n i =
      case (env !? n) >>= (!? i) of 
        Nothing -> "#BVERR(" <> pretty n <> "," <> pretty i <> ")"
        Just a  -> formatDoc a
    
    go env args p (Lam fe) = case fe of
      Abs x body -> go env (x:args) p (unScope body)
      App e1 e2 -> makeLam p env args $ \p' env' ->
        parensIf' (p' > 10) $ go env' [] 10 e1 </> go env' [] 11 e2
      Let defs body -> makeLam p env args $ \p' env' ->
        let env'' = map fst defs : env'
            formatDef (x,rhs)  =
              hang 2 $ formatDoc x <+> "=" </> group (go env' [] 0 rhs)
            defDocs = map formatDef defs
            bodyDoc = go env'' [] 0 (unScope body)
            doc = block "let" defDocs <+> "in" <+> group bodyDoc
        in parensIf' (p' > 9) (group doc)
      LetRec defs body -> makeLam p env args $ \p' env' ->
        let env'' = map fst defs : env'
            formatDef (x,rhs)  =
              hang 2 $ formatDoc x <+> "=" </> group (go env'' [] 0 (unScope rhs))
            defDocs = map formatDef defs
            bodyDoc = go env'' [] 0 (unScope body)
            doc = block "letrec" defDocs <+> "in" <+> group bodyDoc
        in parensIf' (p' > 9) (group doc)
      BV n m -> makeLam p env args $ \_ env' -> lookupEnv env' n m
      FV x    -> makeLam p env args $ \_ _ -> formatDoc x
      Lit a     -> makeLam p env args $ \p' _ -> formatPrec p' a
      Loc _ e   -> go env args p e

    a <#> b = vsep [a, b]
    a </> b = fillSep [a,b]

formatLam :: (Formattable v, Formattable a, Formattable x) =>
             Lam v l a x -> String
formatLam = renderString . customRender . formatLamP 0
