{-# LANGUAGE GADTs #-}
module Data.Lambda.Manipulation.Util(
    OLam,
    foldMapOVs,
    openVars,
    countOV,

    closeLam,
    forceCloseLam,

    LetForm(..),
    fromLet,
    fromLetRec,

    makeLetWith,
    makeLetRecWith,

    toLet,
    toLetRec,
) where

import Data.Bifunctor (Bifunctor(..))
import Data.Monoid ( Sum(..) )

import Data.Map (Map)
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set

import Data.Lambda.Core
import Data.Lambda.Rename ( uniqueRenameBoundVars )
import Data.Lambda.Format (formatLam)

-- | An \"Open\" term. Open term have two kinds of free variables. They are, one we care (@k@)
--   and one we don't care (@x@).
type OLam k v l a x = Lam v l a (Either k x)

-- * Query open variables
foldMapOVs :: (Monoid m) => (k -> m) -> OLam k v l a x -> m
foldMapOVs f = foldMap (either f mempty)

openVars :: (Ord k) => Lam v l a (Either k x) -> Set.Set k
openVars = foldMapOVs Set.singleton

countOV :: (Eq k) => k -> Lam v l a (Either k x) -> Int
countOV k = getSum . foldMapOVs f
  where f j | j == k    = 1
            | otherwise = 0

-- * Make @let@ and @let rec@ of @OLam@ terms, using only
--   opened variables in the definition
makeLetWith, makeLetRecWith :: (Eq k) => 
  (k -> v) -> [(k, OLam k v l a x)] -> OLam k v l a x -> OLam k v l a x
makeLetWith toV defs body = let_ defs' (abstract' isLeftOf ks body)
  where
    ks = map fst defs
    defs' = map (first toV) defs
makeLetRecWith toV defs body = letrec_ defs' (scope body)
  where
    ks = map fst defs
    scope = abstract' isLeftOf ks
    defs' = map (bimap toV scope) defs

-- * Convert back to normal Lam
closeLam :: OLam k v l a x -> Maybe (Lam v l a x)
closeLam = traverse (either (const Nothing) Just)

forceCloseLam :: Lam v l a (Either k x) -> Lam v l a x
forceCloseLam t =
  case closeLam t of
    Nothing -> error $ "forceCloseLam: " ++ formatAnyOLam t
    Just e  -> e

formatAnyOLam :: OLam k v l a x -> String
formatAnyOLam =
  formatLam .
  uniqueRenameBoundVars (const (VarName "x")) (const True) .
  mapFV (VarName . either (const "?left") (const "?right")) .
  mapLit (const ())

-- | @let@ and @let rec@ form, with specially separated type @k@.
data LetForm v l a x where
  LetForm :: (Ord k) => (k -> v) -> Map k (OLam k v l a x) -> OLam k v l a x -> LetForm v l a x

unscope :: Scope (Lam v l a x) -> OLam Int v l a x
unscope scoped = instantiate scoped' fvs
  where scoped' = fmap (fmap Right) scoped
        fvs = fmap (fv_ . Left) [0..]

fromLet :: [(v, Lam v l a x)] -> Scope (Lam v l a x) -> LetForm v l a x
fromLet lets body = LetForm (vs !!) (Map.fromList lets') (unscope body)
  where
    vs = fmap fst lets
    lets' = [ (i, fmap Right rhs) | (i, (_, rhs)) <- zip [0..] lets ]

fromLetRec :: [(v, Scope (Lam v l a x))] -> Scope (Lam v l a x) -> LetForm v l a x
fromLetRec lets body = LetForm (vs !!) (Map.fromList lets') (unscope body)
  where
    vs = fmap fst lets
    lets' = [ (i, unscope rhs) | (i, (_, rhs)) <- zip [0..] lets ]

-- * Common functions

toLet :: LetForm v l a x -> Lam v l a x
toLet (LetForm toV letsMap body) =
  if Map.null letsMap
  then forceCloseLam body
  else forceCloseLam $ makeLetWith toV (Map.toList letsMap) body

toLetRec :: LetForm v l a x -> Lam v l a x
toLetRec (LetForm toV letsMap body) =
  if Map.null letsMap
  then forceCloseLam body
  else forceCloseLam $ makeLetRecWith toV (Map.toList letsMap) body

-- * Commonly used function

isLeftOf :: (Eq v) => v -> Either v x -> Bool
isLeftOf v = either (v==) (const False)

