{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE TypeFamilies #-}
module Data.Lambda.Rename(
  renameCollision, toUniqueNames, uniqueRenameBoundVars
) where

import Control.Monad ( (>=>) )
import Control.Monad.State ( MonadState(state), evalState )
import qualified Data.Set as Set

import Data.VarName
import Data.Lambda.Core

-- | rename bound names to avoid name collision
renameCollision :: (IsName v) => Lam v l a v -> Lam v l a v
renameCollision expr =
  let fvs = freeVars expr
  in uniqueRenameBoundVars id (`Set.notMember` fvs) expr

-- | Make all bound names have unique name.
toUniqueNames :: (IsName v) => Lam v l a x -> Lam v l a x
toUniqueNames = uniqueRenameBoundVars id (const True)

cataM :: (Recursive t, Traversable (Base t), Monad m) => (Base t r -> m r) -> t -> m r
cataM f = cata (sequenceA >=> f)

uniqueRenameBoundVars :: (IsName v') =>
  (v -> v') -> (v' -> Bool) -> Lam v l a x -> Lam v' l a x
uniqueRenameBoundVars f p expr = evalState (cataM step expr) initialState
  where
    fresh v = state (requestNameFor v)
    freshDefs = traverse (\(v,e) -> (,) <$> fresh v <*> pure e)
    initialState = newNameGenerator p

    step fe = case _mapV f fe of
      Abs v body       -> abs_ <$> fresh v <*> pure body
      Let defs body    -> let_ <$> freshDefs defs <*> pure body
      LetRec defs body -> letrec_ <$> freshDefs defs <*> pure body
      fe'              -> return (Lam fe')
