module Data.Lambda.TypeCheck(
  TI,
  typeLam, typeLam'
) where

import           Data.Lambda.Core
import           Data.Lambda.Manipulation
import           Data.TypeCheck
import           Data.TypeCheck.TypeInfer

type TI p v = TypeInfer p v (Type, Subst)

typeLam ::
  (Show v, IsName v) =>
  (l -> TI p v -> TI p v) ->
  (a -> TI p v) ->
  (x -> TI p v) ->
  Lam v l a x -> TI p v
typeLam typeLoc typeLit typeFreeVar t = cata step t' []
  where
    t' = sccAnalysis t

    step fe vars = case fe of
      App e1 e2 -> inferFuncApp (e1 vars) (e2 vars)
      Abs x (Scope body) -> inferFuncAbs x (body ([x]:vars))
      Let defs (Scope body) ->
        let vars' = map fst defs : vars
            defs' = map (\(v,e) -> (NoAnnot v, e vars)) defs
        in inferLets defs' (body vars')
      LetRec defs (Scope body) ->
        let vars' = map fst defs : vars
            defs' = map (\(v,Scope e) -> (NoAnnot v, e vars')) defs
        in inferLetRecs defs' (body vars')
      BV m n    -> inferVar (vars !! m !! n)
      FV x      -> typeFreeVar x
      Lit a     -> typeLit a
      Loc l e   -> typeLoc l (e vars)

typeLam' ::
  (Show v, IsName v) =>
  (a -> TI p v) ->
  Lam v p a v -> TI p v
typeLam' typeLit = typeLam withPos typeLit undefinedVar
