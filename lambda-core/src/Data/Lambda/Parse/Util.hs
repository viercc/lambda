{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.Lambda.Parse.Util(
  -- * Public
  Parser,
  parse',
  ParseError',
  SourceRange(..),
  withRange,

  -- * Private
  greedyMany1,
  space', lexeme', symbol',
  parens', makeIdentifierP, makeCIdentifierP, identifier',
  integer', stringLiteral', charLiteral'
) where

import           Control.Monad

import           Data.Char
import           Data.Void

import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import qualified Text.PrettyPrint.Util      as PP

type Parser = Parsec Void String
type ParseError' = ParseErrorBundle String Void

-- | parse from text
parse' :: Parser e -> String -> Either String e
parse' p str = either (Left . errorBundlePretty) Right $ parse p "" str

-- | longest match
greedyMany1 :: Parser a -> Parser [a]
greedyMany1 p = some p <* notFollowedBy p

-- * lexers
space' :: Parser ()
space' = L.space (void spaceChar)
                 (L.skipLineComment "#")
                 (L.skipBlockCommentNested "[#" "#]")

lexeme' :: Parser a -> Parser a
lexeme' = L.lexeme space'

symbol' :: String -> Parser String
symbol' = L.symbol space'

parens' :: Parser a -> Parser a
parens' = between (symbol' "(") (symbol' ")")

reservedNames :: [String]
reservedNames = ["let", "letrec", "in"]

isIdentChar :: Char -> Bool
isIdentChar c = isAlphaNum c || c == '_' || c == '\''

makeWordP :: (Char -> Bool) -> (Char -> Bool) -> Parser String
makeWordP isLeadChar isBodyChar =
  (:) <$> satisfy isLeadChar <*> many (satisfy isBodyChar) <* notFollowedBy
    (satisfy isBodyChar)

makeIdentifierP :: [String] -> Parser String
makeIdentifierP reserved = try $ lexeme' word
 where
  word = do
    x <- makeWordP isLower isIdentChar
    guard (x `notElem` reserved)
    return x

makeCIdentifierP :: [String] -> Parser String
makeCIdentifierP reserved = try $ lexeme' word
 where
  word = do
    x <- makeWordP isUpper isIdentChar
    guard (x `notElem` reserved)
    return x

identifier' :: Parser String
identifier' = makeIdentifierP reservedNames

integer' :: Parser Integer
integer' = lexeme' (L.signed (return ()) (L.decimal <|> L.hexadecimal))

stringLiteral' :: Parser String
stringLiteral' = lexeme' (char '"' *> manyTill L.charLiteral (char '"'))

charLiteral' :: Parser Char
charLiteral' = lexeme' (char '\'' *> L.charLiteral <* char '\'')

-- * position
data SourceRange = SourceRange SourcePos SourcePos
                 | BuiltinPos String
                 deriving (Show, Eq, Ord)

instance PP.Pretty SourceRange where
  pretty (SourceRange (SourcePos fileName l1 c1) (SourcePos _ l2 c2)) =
    let spanDoc | l1 == l2  = "Line" <+> PP.pretty (show l1) <+> ":" <+>
                              "Column" <+> PP.pretty (show c1) <> "-" <> PP.pretty (show c2)
                | otherwise = "Line" <+> PP.pretty (show l1) <+> "-" <+> "Line" <+> PP.pretty (show l2)
    in "at" <+> PP.pretty fileName <+> PP.parens spanDoc
      where (<+>) = (PP.<+>)
            (<>) = (PP.<>)
  pretty (BuiltinPos str) = "at" PP.<+> PP.pretty str

withRange :: (SourceRange -> a -> a) -> Parser a -> Parser a
withRange f p = wrap <$> getSourcePos <*> p <*> getSourcePos
  where wrap p1 e p2 = f (SourceRange p1 p2) e
