{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Data.Lambda.Parse.Custom(
  PreParser(..),
  defaultPreParser, defaultPreParser',
  buildLamP,

  Decorator,
  locExtension,
  noExtension
) where

import           Control.Monad

import           Data.String            (IsString (fromString))

import           Data.Lambda.Parse.Util
import           Text.Megaparsec

import           Data.Lambda.Core

-- * Parser customization

-- | Components of the parser, which has broken recursive reference
--   each other (aka "open recursion").
data PreParser v l a x = PreParser
  { litP       :: Parser a
  , varP       :: Parser v
  , fvarP      :: Parser x
  , preAbsP    :: PreParser v l a x -> Parser (Lam v l a x)
  , preTermP   :: PreParser v l a x -> Parser (Lam v l a x)
  , preAppsP   :: PreParser v l a x -> Parser (Lam v l a x)
  , preLetP    :: PreParser v l a x -> Parser (Lam v l a x)
  , preLetRecP :: PreParser v l a x -> Parser (Lam v l a x)
  , preExprP   :: PreParser v l a x -> Parser (Lam v l a x)
  }

-- | Make complete parser from PreParser
buildLamP :: PreParser v l a x -> Parser (Lam v l a x)
buildLamP pc = space' *> preExprP pc pc

-- | Default PreParser
defaultPreParser :: (IsString v, Eq v) =>
  PreParser v l a v
defaultPreParser = defaultPreParser' (==)

defaultPreParser' :: (IsString v, IsString x) =>
  (v -> x -> Bool) -> PreParser v l a x
defaultPreParser' is = PreParser
  { litP = mzero
  , varP = fromString <$> identifier'
  , fvarP = fromString <$> identifier'
  , preAbsP = \pc ->
      abstMulti <$> (symbol' "\\" *> some (varP pc))
                <*> (symbol' "->" *> preExprP pc pc) <?> "abs"
  , preTermP = \pc ->
          fv_ <$> fvarP pc
      <|> lit_ <$> litP pc
      <|> preAbsP pc pc
      <|> parens' (preExprP pc pc)
  , preAppsP = \pc ->
      foldl1 app_ <$> greedyMany1 (preTermP pc pc) <?> "apps"
  , preLetP = \pc ->
      makeLet' is
        <$> (symbol' "let" *> symbol' "{" *> defsP pc <* symbol' "}")
        <*> (symbol' "in" *> preExprP pc pc)
        <?> "let"
  , preLetRecP = \pc ->
      makeLetRec' is
        <$> (symbol' "letrec" *> symbol' "{" *> defsP pc <* symbol' "}")
        <*> (symbol' "in" *> preExprP pc pc)
        <?> "letrec"
  , preExprP = \pc ->
      try (preLetRecP pc pc) <|> try (preLetP pc pc) <|> preAppsP pc pc
      <?> "expr"
  }
  where
    abstMulti xs body = foldr (makeAbs' is) body xs
    defsP pc = sepBy1 (defP pc) (symbol' ";")
    defP pc = (,) <$> (varP pc <* symbol' "=") <*> preExprP pc pc

-- * Extensions

-- | Modify PreParser in specific way.
type Decorator v l a x = PreParser v l a x -> PreParser v l a x

-- | Annotate parser with position in source
locExtension :: Decorator v SourceRange a x
locExtension pc =
  pc{ preTermP = addLoc . preTermP pc
    , preAppsP = addLoc . preAppsP pc
    , preLetP = addLoc . preLetP pc
    , preLetRecP = addLoc . preLetRecP pc }
  where
    -- Wrap parse result with `loc_`, but if parse result was
    -- already wrapped, do not add more.
    addLoc = withRange $ \pos term ->
      case term of
        Lam Loc{} -> term
        _         -> loc_ pos term

-- | Use no extension at all.
noExtension :: Decorator v l a x
noExtension = id
