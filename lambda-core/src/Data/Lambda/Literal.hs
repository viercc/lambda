{-# LANGUAGE DeriveLift            #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE EmptyCase #-}
module Data.Lambda.Literal(
   Literal(..), NoLiteral(), absurdLiteral
) where

import           Language.Haskell.TH.Syntax (Lift)

import Text.PrettyPrint.Formattable
import qualified Text.PrettyPrint.Util as PP

-- | Literals embeddable in lambda expressions.
data Literal = IntegerLit Integer
             | StringLit String
             | CharLit Char
             deriving (Show, Eq, Lift)

instance Formattable Literal where
  formatPrec _ lit = case lit of
    IntegerLit intLit -> PP.pretty (show intLit)
    StringLit strLit  -> PP.pretty (show strLit)
    CharLit charLit   -> PP.pretty (show charLit)

-- | Monomorphic type which represents there is no literal in lambda expressions.
--   Isomorphic to @Void@.
data NoLiteral
  deriving (Lift)

instance Show NoLiteral where
  show = absurdLiteral

instance Eq NoLiteral where
  (==) = absurdLiteral

instance Ord NoLiteral where
  compare = absurdLiteral

instance Formattable NoLiteral where
  formatPrec _ = absurdLiteral

absurdLiteral :: NoLiteral -> a
absurdLiteral = error "There should not exist a value of NoLiteral"
