{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE GADTs                      #-}
module Data.Lambda.Manipulation (
  sccAnalysis, floatLetOutward, floatLetInward
) where

import Data.Bifunctor (Bifunctor(..))
import Data.Foldable (foldl')
import Data.Monoid
import qualified Data.Map.Strict               as Map
import qualified Data.Set                      as Set
import Data.Graph (stronglyConnComp, SCC(..))

import           Data.Lambda.Core
import           Data.Lambda.Format            ()
import           Data.Lambda.Util
import           Data.Lambda.Rename ( toUniqueNames )
import           Data.Lambda.Manipulation.Util

floatLetOutward :: (IsName v) => Lam v l a x -> Lam v l a x
floatLetOutward = finish . cata step . toUniqueNames
  where
    cannotFloat = occursBVSatisfying (\d _ -> d == 0)

    makeLetRec'' defs body
      | Map.null defs = body
      | otherwise     = makeLetRec' isLeftOf (Map.toList defs) body

    finish (defs, body) = forceCloseLam $ makeLetRec'' defs body

    step fe = case fe of
      App (def1, e1) (def2, e2) -> (Map.union def1 def2, app_ e1 e2)
      Abs v (Scope (defs, e)) ->
        let directlyStrapped = map fst . filter (cannotFloat . snd) $ Map.toList defs
            dependencyGraph = openVars <$> defs
            strapped = reachableTo dependencyGraph directlyStrapped
            (defStrapped, defFloatable) =
              Map.partitionWithKey (\v' _ -> v' `Set.member` strapped) defs
            body' = makeLetRec'' defStrapped e
        in (shift (-1) <$> defFloatable, abs_ v (Scope body'))
      Let defs (Scope (defBody, body)) ->
        let fvs = fv_ . Left . fst <$> defs
            defs' = foldMap (\(v, (defdef, defrhs)) -> Map.insert v defrhs defdef) defs
            defBody' = fmap (\rhs -> instantiate (Scope rhs) fvs) defBody
            body' = instantiate (Scope body) fvs
        in (Map.union defs' defBody', body')
      LetRec defs (Scope (defBody, body)) ->
        let fvs = fv_ . Left . fst <$> defs
            unscope' e = instantiate (Scope e) fvs
            defs' = foldMap (\(v, Scope (defdef, defrhs)) ->
                Map.insert v (unscope' defrhs) (unscope' <$> defdef)
              ) defs
            defBody' = unscope' <$> defBody
            body' = unscope' body
        in (Map.union defs' defBody', body')
      BV d n -> (Map.empty, bv_ d n)
      FV x   -> (Map.empty, fv_ (Right x))
      Lit a  -> (Map.empty, lit_ a)
      Loc m e -> loc_ m <$> e

floatLetInward :: (Ord v) => Lam v l a x -> Lam v l a x
floatLetInward = cata step
  where
    step fe = case fe of
      Let defs body ->
        case fromLet defs body of
          LetForm toV defsMap body' ->
            forceCloseLam $ Map.foldrWithKey (floatLetInwardStep toV) body' defsMap
      LetRec defs body ->
        case fromLetRec defs body of
          LetForm toV defsMap body' ->
            forceCloseLam $ floatLetRecInwardStep toV (Map.toList defsMap) body'
      _ -> Lam fe

data UsageLevel = NoUse | LocalUse | WideUse
  deriving (Show, Read, Eq, Ord)

insertDefToLet :: (Eq k) => k -> v -> OLam k v l a x -> [(v, OLam k v l a x)] -> OLam k v l a x -> OLam k v l a x
insertDefToLet k v rhs defs body =
  let step d fe = case fe of
        FV x   | k `isLeftOf` x -> bv_ d 0
        BV n i | d == n      -> bv_ d (i+1)
        _      -> Lam fe
      body' = foldWithDepth step body
  in let_ ((v, rhs) : defs) (Scope body')

floatLetInwardStep :: (Ord k)
  => (k -> v) -> k -> OLam k v l a x -> OLam k v l a x -> OLam k v l a x
floatLetInwardStep toV k rhs = result . foldWithDepth step
  where
    v = toV k
    wrap d = makeLetWith toV [(k, shift d rhs)]

    level (l,_,_)    = l
    original (_,e,_) = e
    result (_,_,r)   = r

    countUsage = getSum . foldMap (Sum . indicate . (/= NoUse) . level)
    
    indicate :: Bool -> Int
    indicate b = if b then 1 else 0

    step d fe =
      case fe of
        BV _ _ -> (NoUse, e, e)
        FV x   -> if k `isLeftOf` x then (LocalUse, e, shift d rhs) else (NoUse, e, e)
        Lit _  -> (NoUse, e, e)
        Abs _ (Scope body) | level body == NoUse -> (NoUse, e, e)
                           | otherwise           -> (WideUse, e, wrap d e)
        Let defs (Scope body) | usage == 1 && level body == WideUse ->
          let originalDefs = map (fmap original) defs
          in (LocalUse, e, insertDefToLet k v (shift d rhs) originalDefs (original body))
        _ -> case usage of
               0 -> (NoUse, e, e)
               1 -> (LocalUse, e, r)
               _ -> (WideUse, e, wrap d e)
      where
        e = Lam $ original <$> fe
        r = Lam $ result <$> fe

        usage = countUsage fe

floatLetRecInwardStep :: (Ord k)
  => (k -> v)
  -> [(k, OLam k v l a x)] -> OLam k v l a x -> OLam k v l a x
floatLetRecInwardStep toV fdefs = result . foldWithDepth step
  where
    wrap d = makeLetRecWith toV (map (second (shift d)) fdefs)

    vs = Set.fromList (map fst fdefs)
    isBound (Left v)  = v `Set.member` vs
    isBound (Right _) = False

    level (l,_,_)    = l
    original (_,e,_) = e
    result (_,_,r)   = r

    countUsage = getSum . foldMap (Sum . indicate . (/= NoUse) . level)
    
    indicate :: Bool -> Int
    indicate b = if b then 1 else 0

    step d fe =
      case fe of
        BV _ _ -> (NoUse, e, e)
        FV x   -> if isBound x then (WideUse, e, wrap d e) else (NoUse, e, e)
        Lit _  -> (NoUse, e, e)
        Abs _ (Scope body) | level body == NoUse -> (NoUse, e, e)
                           | otherwise           -> (WideUse, e, wrap d e)
        _ -> case countUsage fe of
               0 -> (NoUse, e, e)
               1 -> (LocalUse, e, r)
               _ -> (WideUse, e, wrap d e)
      where
        e = Lam $ original <$> fe
        r = Lam $ result <$> fe

-- | Analyze dependency between @letrec@ definitions and break down to nested small
--   @let@ or @letrec@ terms
sccAnalysis :: Lam v l a x -> Lam v l a x
sccAnalysis = cata step
  where
    step (LetRec lets body) = decomposeLetRec $ fromLetRec lets body
    step e                  = Lam e

decomposeLetRec :: LetForm v l a x -> Lam v l a x
decomposeLetRec (LetForm toV letsMap body) = forceCloseLam $ foldr wrap body sccList
  where
    depGraph = [ ((v, rhs), v, Set.toList (openVars rhs)) | (v,rhs) <- Map.toList letsMap ]
    sccList = stronglyConnComp depGraph

    wrap (AcyclicSCC def) term = makeLetWith toV [def] term
    wrap (CyclicSCC defs) term = makeLetRecWith toV defs term

isLeftOf :: (Eq v) => v -> Either v x -> Bool
isLeftOf v = either (v==) (const False)


-- * Utilities

type Rel a b = Map.Map a (Set.Set b)

reachableFrom :: (Ord a) => Rel a a -> [a] -> Set.Set a
reachableFrom graph = loop Set.empty
  where
    loop acc [] = acc
    loop acc (a:as)
      | a `Set.member` acc = loop acc as
      | otherwise          =
          let next = Set.toList $ Map.findWithDefault Set.empty a graph
          in loop (Set.insert a acc) (next ++ as)

reachableTo :: (Ord a) => Rel a a -> [a] -> Set.Set a
reachableTo = reachableFrom . transpose

transpose :: (Ord a, Ord b) => Rel a b -> Rel b a
transpose = foldl' (Map.unionWith Set.union) Map.empty . map tr1 . Map.toList
  where
    tr1 (a, bs) = Map.fromDistinctAscList [ (b, Set.singleton a) | b <- Set.toAscList bs]
