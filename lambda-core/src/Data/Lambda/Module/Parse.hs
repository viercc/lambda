{-# LANGUAGE FlexibleContexts #-}
module Data.Lambda.Module.Parse(
  -- | Default module parser
  moduleP,
  -- | Return type of default module parser
  ParsedModule,

  -- | Build custom parser module
  buildModuleP,

  -- * Parser Components
  defaultQVarP, moduleName, qualifiedFreeVar,
  defaultPreParserForModule,
) where

import           Data.Char
import           Data.String                  (IsString (..))

import qualified Data.Map.Strict              as Map

import           Text.PrettyPrint.Formattable (Formattable (..), format)

import           Data.Lambda.Parse.Util
import           Text.Megaparsec
import           Text.Megaparsec.Char

import           Data.Lambda.Core
import           Data.Lambda.Module.Core
import           Data.Lambda.Parse.Custom

moduleName :: Parser ModuleName
moduleName = try $
    (:) <$> satisfy isUpper <*> many identChar <* notFollowedBy identChar
  where
    identChar = satisfy (\c -> isAlphaNum c || c == '_' || c == '\'')

defaultQVarP :: Parser v -> Parser (Qualified v)
defaultQVarP unqVarP = mkQVar <$> optional (moduleName <* char '.') <*> unqVarP
  where mkQVar Nothing x  = Unqualified x
        mkQVar (Just m) x = Qualified m x

qualifiedFreeVar ::
  Decorator v l a (Qualified v)
qualifiedFreeVar pc =
  pc{ fvarP = defaultQVarP (varP pc) }

defaultPreParserForModule ::
  (IsString v, Eq v) => PreParser v l a (Qualified v)
defaultPreParserForModule =
  qualifiedFreeVar $ defaultPreParser' (\v x -> Unqualified v == x)

buildModuleP :: (Ord v, Formattable v) =>
  Parser t -> PreParser v l a (Qualified v) -> Parser (Module t v l a)
buildModuleP annotP pc =
  mkModule <$  space'
           <*  symbol' "module" <*> moduleNameP <*> tupled (varP pc)
           <*> requiresP
           <*> definitionsP
           <*  eof
  where
    mkModule modName exports requires definitions =
      Module{ _moduleName = modName
            , _requires = requires
            , _exports = exports
            , _definitions = definitions }

    moduleNameP = lexeme' moduleName
    semi = symbol' ";"
    requiresP = option [] $ symbol' "requires" *> some (moduleNameP <* semi)
    definitionsP = Map.fromList <$ symbol' "definitions" <*> many definitionP
    definitionP =
      do lhs <- varP pc
         maybeAnnot <- optional $
           symbol' "::" *> annotP <* semi <* symbol' (format lhs)
         rhs <- symbol' "=" *> preExprP pc pc <* semi
         return (lhs, (maybeAnnot, rhs))
    tupled p = parens' (sepEndBy1 p (symbol' ","))

type ParsedModule t a = Module t VarName SourceRange a

moduleP :: Parser t -> Parser a -> Parser (ParsedModule t a)
moduleP annotP literalP =
  buildModuleP annotP $ locExtension $ defaultPreParserForModule{ litP = literalP }
