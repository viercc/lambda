module Data.Lambda.Module.TypeCheck (
  typeCheckToplevel,
  FlatDefinitions
) where

import qualified Data.Map.Strict          as Map
import qualified Data.Set as Set
import           Data.Graph (stronglyConnComp, SCC(..))

import           Data.Lambda.Core         as Lam

import           Data.Lambda.TypeCheck
import           Data.TypeCheck
import           Data.TypeCheck.TypeInfer

import           Data.Lambda.Module.Loader (FlatDefinitions)

-- | Perform type check against each toplevel definitions
--   which may be recursively defined and may have type
--   annotations.
--
--   You must give a way to type infer an lambda expression.
--
--   This is intended to apply on the output of
--   'Data.Lambda.Module.Loader.mergeModules'.
typeCheckToplevel ::
  (Show v, IsName v) =>
  (l -> TI p v -> TI p v) ->
  (a -> TI p v) ->
  FlatDefinitions TypeScheme v l a ->
  TypeInfer p v (TypeEnv v)
typeCheckToplevel typeExt typeLit defs =
  let graph = [ (v, v, Set.toList (Lam.freeVars rhs)) | (v, (_, rhs)) <- Map.toList defs ]
      sccs = stronglyConnComp graph
  in foldr step getEnv sccs
  where
    typeLamQ = typeLam typeExt typeLit inferVar  
    getDef v =
      case defs Map.! v of
        (Nothing, lamExpr) -> (NoAnnot v, typeLamQ lamExpr)
        (Just ts, lamExpr) -> (Annot v ts, typeLamQ lamExpr)
    step (AcyclicSCC v) rest =
      do (env,_) <- inferLetDefs [getDef v]
         localEnv (const env) rest
    step (CyclicSCC vs) rest =
      do (env,_) <- inferLetRecDefs (map getDef vs)
         localEnv (const env) rest
