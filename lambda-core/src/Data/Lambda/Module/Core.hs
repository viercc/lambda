{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
module Data.Lambda.Module.Core (
  ModuleName, Qualified(..),
  Definitions,
  Module(..)
) where

import Data.String (IsString(..))

import Data.Map.Strict (Map)

import Text.PrettyPrint.Formattable
import qualified Text.PrettyPrint.Util as PP

import Data.VarName
import Data.Lambda.Core

type ModuleName = String
data Qualified v = Qualified ModuleName v
                 | Unqualified v
  deriving (Show, Read, Eq, Ord)

-- | Never provide @Formattable (Qualified v)@ instance for
--   general @v@, as syntax for them varies.
instance Formattable (Qualified VarName) where
  formatPrec _ qv = case qv of
    Qualified modName v -> PP.pretty modName <> PP.dot <> formatDoc v
    Unqualified v       -> formatDoc v

instance IsString v => IsString (Qualified v) where
  fromString = Unqualified . fromString

instance IsName v => IsName (Qualified v) where
  uniqueNames = Unqualified <$> uniqueNames
  derive (Qualified modName v) = Qualified modName <$> derive v
  derive (Unqualified v) = Unqualified <$> derive v

type Definitions t v f a =
  Map v (Maybe t, Lam v f a (Qualified v))

data Module t v f a = Module
  { _moduleName :: ModuleName
  , _requires :: [ModuleName]
  , _exports :: [v]
  , _definitions :: Definitions t v f a }
