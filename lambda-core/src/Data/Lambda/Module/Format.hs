{-# LANGUAGE FlexibleContexts #-}
module Data.Lambda.Module.Format(
  formatDocModule
) where

import qualified Data.Map.Strict               as Map

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Data.Lambda.Format(formatLamP)
import           Data.Lambda.Module.Core

formatDocModule ::
  (Formattable t, Formattable v, Formattable (Qualified v),
   Formattable a) => Module t v l a -> Doc ann
formatDocModule m =
  pretty "module" <+> pretty modName <+> tupled (map formatDoc exports) <#>
  requiresDoc <#>
  defsDoc
  where
    modName = _moduleName m
    exports = _exports m
    requires = _requires m
    defs = Map.toList (_definitions m)

    requiresDoc | null requires = mempty
                | otherwise =
      pretty "requires" <#>
      indent 2 (vsep $ map (\req -> pretty req <> semi) requires)

    defsDoc =
      pretty "definitions" <#>
      indent 2 (vsep $ map formatOneDef defs)
    formatOneDef (v, (annot, expr)) =
      formatAnnot v annot $
      formatDoc v <+> pretty "=" <+> formatLamP 0 expr <> semi

    formatAnnot _ Nothing body = body
    formatAnnot v (Just t) body =
      formatDoc v <+> pretty "::" <+> formatDoc t <> semi <#> body

    a <#> b = vsep [a,b]
