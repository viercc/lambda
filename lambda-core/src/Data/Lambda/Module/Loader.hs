{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE UndecidableInstances       #-}
module Data.Lambda.Module.Loader (
  -- * Generic module loader
  loadModules,

  -- * Flatten multiple modules
  mergeModules,
  FlatDefinitions,

  -- * Loading modules in IO
  parseModuleFileIO,
  loadModulesIO,

  -- * Related types
  ModuleLoadErrorItem(..),
  ModuleLoadError(..),
  Load
) where

import           Control.Exception
import           System.FilePath.Posix
import           System.IO.Error              (isDoesNotExistError)

import           Data.Bifunctor
import           Data.Either.Validation
import           Data.Map.Strict              (Map)
import qualified Data.Map.Strict              as Map

import           Text.PrettyPrint.Formattable
import qualified Text.PrettyPrint.Util        as PP

import           Data.Lambda.Parse.Util       (ParseError', Parser)
import qualified Text.Megaparsec              as Parser

import           Data.Lambda.Core
import           Data.Lambda.Module.Core

data ModuleLoadErrorItem v =
    FileNotFound
  | ReadFileError SomeException
  | ParseError ParseError'
  | WrongModuleName FilePath ModuleName
  | NameResolveError [NameResolveError v]
  deriving (Show)

instance (Formattable v, Formattable (Qualified v)) =>
         PP.Pretty (ModuleLoadErrorItem v) where
  pretty ei = case ei of
    FileNotFound    -> PP.pretty "FileNotFound"
    ReadFileError e -> PP.pretty (displayException e)
    ParseError e    -> PP.pretty (Parser.errorBundlePretty e)
    WrongModuleName path modName ->
      PP.pretty (show path) PP.<+>
      PP.pretty "defines wrongly named module:" PP.<+>
      PP.pretty modName
    NameResolveError nres -> PP.pretty nres

newtype ModuleLoadError v =
  MLE (Map ModuleName [ModuleLoadErrorItem v])
  deriving (Show, Semigroup, Monoid)

instance (Formattable v, Formattable (Qualified v)) =>
         PP.Pretty (ModuleLoadError v) where
  pretty (MLE e) = PP.vsep $ ppOneModule <$> Map.toList e
    where
      ppOneModule (_, []) = mempty
      ppOneModule (moduleName, errors) =
        PP.fillSep [ PP.pretty moduleName PP.<> PP.colon
                  , PP.indent 2 (PP.vsep $ map PP.pretty errors)
                  ]


singleMLE :: ModuleName -> ModuleLoadErrorItem v -> ModuleLoadError v
singleMLE modName err = MLE $ Map.singleton modName [err]

type Load v m a = m (Validation (ModuleLoadError v) a)

-- | Load a module and all modules it requires transitively.
--   It uses passed function to load single module.
--   Recursively required modules are permitted.
loadModules ::
  (Monad m, Ord v) =>
  (ModuleName -> Load v m (Module t v l a)) ->
  ModuleName -> Load v m (Map ModuleName (Module t v l a))
loadModules parseModuleFileM root = loop Map.empty [root]
  where
    loop env [] = return $
      case sequenceA env of
        Failure e    -> Failure e
        Success env' -> resolveNameAllModules env'
    loop env (modName:rest) =
      if modName `Map.member` env
        then loop env rest
        else do result <- parseModuleFileM modName
                let env' = Map.insert modName result env
                loop env' (foldMap _requires result ++ rest)

type FlatDefinitions t v l a =
  Map v (Maybe t, Lam v l a v)

-- | Merge modules loaded by 'loadModules' to flat set of definitions.
--   Each defined variable are qualified by the name of module it belongs.
mergeModules ::
  (Ord v) =>
  Map ModuleName (Module t v l a) ->
  FlatDefinitions t (Qualified v) l a
mergeModules = foldMap flatDefs
  where
    flatDefs m =
      let modName = _moduleName m
          defs = _definitions m
          convertDef = second (mapBoundVarName Unqualified)
          qualifyLHS = Qualified modName
      in Map.mapKeysMonotonic qualifyLHS $ Map.map convertDef defs


-- * Simple loader over IO Monad

-- | Parse the content of a file. The result is passed to three-way
--   continuation: for IOError, for parse error, and for success.
parseFileIO ::
  (IOError -> r) -> (ParseError' -> r) -> (a -> r) -> Parser a -> FilePath -> IO r
parseFileIO handleIOError handleParseError success p filePath =
  catch parseFileIO' (return . handleIOError)
  where
    parseFileIO' =
      do src <- readFile filePath
         evaluate $
           either handleParseError success $ Parser.parse p filePath src

-- | Parse a module with given parser.
--   Partially applied this function can be passed to @loadModules@ function.
--
--   To load a module with name @AnModule@, it reads the file \"AnModule.ext\"
--   where \"ext\" is the @String@ passed as second argument.
parseModuleFileIO :: Parser (Module t v l a) -> String ->
                     ModuleName -> Load v IO (Module t v l a)
parseModuleFileIO moduleP fileExt modName =
  parseFileIO handleIOE handleParseE validateModName moduleP modFile
  where
    modFile = modName <.> fileExt
    singleError = singleMLE modName
    handleIOE e
      | isDoesNotExistError e = Failure . singleError $ FileNotFound
      | otherwise             = Failure . singleError $ ReadFileError (toException e)
    handleParseE = Failure . singleError . ParseError
    validateModName parsed =
      if _moduleName parsed == modName
        then Success parsed
        else Failure . singleError $ WrongModuleName modFile (_moduleName parsed)

-- | > loadModulesIO moduleP fileExt = loadModules (parseModuleFileIO moduleP fileExt)
loadModulesIO ::
  (Ord v) =>
  Parser (Module t v l a) -> String ->
  ModuleName -> Load v IO (Map ModuleName (Module t v l a))
loadModulesIO moduleP fileExt = loadModules (parseModuleFileIO moduleP fileExt)


-- * Name Resolving

newtype VarNameEnv v = VNE (Map v [ModuleName])

varNameEnv :: (Ord v) => Module t v l a -> VarNameEnv v
varNameEnv m = makeVNE (_moduleName m) (_exports m)

makeVNE :: (Ord v) => ModuleName -> [v] -> VarNameEnv v
makeVNE modName varNames =
  VNE $ Map.fromList $ map (\v -> (v, [modName])) varNames

appendVNE :: (Ord v) => VarNameEnv v -> VarNameEnv v -> VarNameEnv v
appendVNE (VNE env1) (VNE env2) = VNE $ Map.unionWith (++) env1 env2

catVNE :: (Ord v) => [VarNameEnv v] -> VarNameEnv v
catVNE = foldr appendVNE (VNE Map.empty)

data NameResolveError v = Ambiguous [Qualified v]
                        | Undefined v
  deriving (Show, Read, Eq, Ord)

instance (Formattable v, Formattable (Qualified v)) =>
         PP.Pretty (NameResolveError v) where
  pretty nre = case nre of
    Ambiguous vs -> PP.pretty "Ambiguous among:" PP.<+> formatDoc vs
    Undefined v  -> PP.pretty "Undefined variable:" PP.<+> formatDoc v

type ResolveResult v = Validation [NameResolveError v]

resolveName :: (Ord v) =>
  VarNameEnv v -> v -> ResolveResult v (Qualified v)
resolveName (VNE env) v = case Map.lookup v env of
  Nothing  -> Failure [Undefined v]
  Just [m] -> Success (Qualified m v)
  Just ms  -> Failure [Ambiguous (fmap (\m -> Qualified m v) ms)]

resolveNameLam :: (Ord v) =>
  VarNameEnv v -> Lam v l a (Qualified v) ->
  ResolveResult v (Lam v l a (Qualified v))
resolveNameLam env = traverse checkFV
  where
    checkFV (Unqualified v) = resolveName env v
    checkFV qv              = Success qv

resolveNameModule :: (Ord v) =>
  (ModuleName -> VarNameEnv v) -> Module t v l a ->
  ResolveResult v (Module t v l a)
resolveNameModule getEnv m =
  let modName = _moduleName m
      requires = _requires m
      exports = _exports m
      defs = _definitions m
      envReq = catVNE $ map getEnv requires
      envThis = makeVNE modName (Map.keys defs)
      env = appendVNE envThis envReq
      checkExport v =
        if Map.member v defs then Success v else Failure [Undefined v]
      checkDef (annot, t) = (,) annot <$> resolveNameLam env t
  in Module modName requires
       <$> traverse checkExport exports
       <*> traverse checkDef defs

resolveNameAllModules ::
  (Ord v) =>
  Map ModuleName (Module t v l a) ->
  Validation (ModuleLoadError v) (Map ModuleName (Module t v l a))
resolveNameAllModules env =
  let nameEnvMap = Map.map varNameEnv env
      getEnv = (nameEnvMap Map.!)
      f m = case resolveNameModule getEnv m of
        Failure err -> Failure . MLE $ Map.singleton (_moduleName m) [NameResolveError err]
        Success m'  -> Success m'
  in traverse f env
