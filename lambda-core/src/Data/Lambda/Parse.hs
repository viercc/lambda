{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Data.Lambda.Parse(
  Parser, parse', SourceRange(..),

  pureLamP,
  ParsedLam, lamP,

  literalP, noLiteralP,
  varNameP
) where

import           Control.Monad

import           Data.Lambda.Parse.Custom
import           Data.Lambda.Parse.Util
import           Text.Megaparsec

import           Data.Lambda.Core
import           Data.Lambda.Literal

-- * Pre-customized parsers
pureLamP :: Parser (Lam' VarName VarName)
pureLamP = buildLamP defaultPreParser

type ParsedLam a = Lam VarName SourceRange a VarName

lamP :: Parser a -> Parser (ParsedLam a)
lamP givenLitP = buildLamP $
  locExtension defaultPreParser{ litP = givenLitP }

-- * Parsers
literalP :: Parser Literal
literalP = intLit <|> strLit <|> charLit where
    intLit  = IntegerLit <$> integer' <?> "integer"
    strLit  = StringLit <$> stringLiteral' <?> "string"
    charLit = CharLit <$> charLiteral' <?> "char"

noLiteralP :: Parser NoLiteral
noLiteralP = mzero

varNameP :: Parser VarName
varNameP = VarName <$> identifier'
