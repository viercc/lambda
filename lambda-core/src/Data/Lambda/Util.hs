{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs         #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE BangPatterns #-}
module Data.Lambda.Util(
  -- * Higher order syntax
  S(..),
  lam, lam2, lam3,
  succEq,

  -- * Bound variable occurence
  isProper, countBV, occursBV,
  occursBVSatisfying,
  foldMapBVWithDepth
) where

import           Data.String

import           Data.Semigroup

import           Data.Lambda.Core

-- * Higher order syntax

data S a = Z | S a
  deriving (Functor, Show, Read, Eq, Ord)

instance IsString a => IsString (S a) where
  fromString = S . fromString

instance IsName a => IsName (S a) where
  uniqueNames = S <$> uniqueNames
  derive Z     = uniqueNames
  derive (S v) = S <$> derive v

lam :: 
  v -> (Lam v l a (S x) -> Lam v l a (S x)) ->
  Lam v l a x
lam v f =
  fromS <$> makeAbs' (\_ x -> isZ x) v (f (fv_ Z))
  where isZ Z     = True
        isZ (S _) = False
        fromS Z     = error "should not happen"
        fromS (S a) = a

lam2 :: (expr ~ Lam v l a (S (S x))) =>
  v -> v -> (expr -> expr -> expr) ->
  Lam v l a x
lam2 v1 v2 f =
  lam v1 $ \x1 ->
  lam v2 $ f (S <$> x1)

lam3 :: (expr ~ Lam v l a (S (S (S x)))) =>
  v -> v -> v -> (expr -> expr -> expr -> expr) ->
  Lam v l a x
lam3 v1 v2 v3 f =
  lam v1 $ \x1 ->
  lam v2 $ \x2 ->
  lam v3 $ f (S . S <$> x1) (S <$> x2)

succEq :: (a -> b -> Bool) -> (a -> S b -> Bool)
succEq _ _ Z      = False
succEq eq a (S b) = eq a b

-- * Bounded variable manipulation

-- | No bounded variable points outside of given expression
isProper :: Lam v l a x -> Bool
isProper = getAll . foldMapBVWithDepth deeper where
  deeper d n _ = All $ d > n

-- | Count occurence of bounded variable.
countBV :: Int -> Int -> Lam v l a x -> Int
countBV n m = getSum . foldMapBVWithDepth f
  where f d n' m' = if d + n == n' && m == m' then Sum 1 else Sum 0

-- | Check occurence of bounded variable.
occursBV :: Int -> Int -> Lam v l a x -> Bool
occursBV n m = occursBVSatisfying (\n' m' -> n == n' && m == m')

occursBVSatisfying :: (Int -> Int -> Bool) -> Lam v l a x -> Bool
occursBVSatisfying predicate = getAny . foldMapBVWithDepth f
  where f d n m = Any (predicate (n-d) m)

foldMapBVWithDepth ::
  (Monoid m) => (Int -> Int -> Int -> m) -> Lam v l a x -> m
foldMapBVWithDepth f t = go t 0
  where
    go (Lam fe) = case fe of
      App e1 e2        -> go e1 <> go e2
      Abs _ body       -> go' body
      Let defs body    -> foldMap (go . snd) defs <> go' body
      LetRec defs body -> foldMap (go' . snd) defs <> go' body
      BV n i   -> \d -> f d n i
      FV _     -> mempty
      Lit _    -> mempty
      Loc _ e  -> go e
    go' (Scope e) d = let !d' = d + 1 in go e d'
