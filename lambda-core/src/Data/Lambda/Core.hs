{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE TypeFamilies      #-}
module Data.Lambda.Core (
  -- * Depending Module
  module Data.Functor.Void1,
  module Data.Functor.Foldable,

  -- * Core types
  Lam(..),
  LamF(..),
  Scope(..),

  -- * Variable names
  module Data.VarName,

  -- * Mapping
  mapBoundVarName, mapLoc, mapLit, mapFV,
  
  -- * Pure lambda calculus
  NoLiteral,
  pureToAny, anyLiteral, anyLoc,
  Lam',
  
  -- * Construction
  app_, abs_, let_, letrec_, bv_, fv_, lit_, loc_,
  (|$|),
  makeFV,
  makeAbs, makeLet, makeLetRec,
  makeAbs', makeLet', makeLetRec',
  
  -- * Free variables
  freeVars,
  replaceFV,
  countFV,

  -- * Annotations
  removeLoc,
  foldWithLoc,
  traverseFVsWithLoc,
  freeVarsWithLoc,
  
  -- * Primitive operations
  foldWithDepth,
  walk, walkFV, walkLoc, walkLit,
  shift, instantiate, abstract,
  abstract',

  viewFV, viewLit, viewLoc,
  _mapV, _mapL, _mapA, _mapFV
) where

import           Control.Monad          (ap)
import           Data.Functor.Classes
import           Data.Functor.Void1

import           Data.String            (IsString (fromString))

import           Data.List              (findIndex)
import           Data.Semigroup
import qualified Data.Set               as Set

import           Data.Bifoldable
import           Data.Bifunctor
import           Data.Bitraversable

import           Data.Functor.Foldable hiding (fold)

import           Data.Lambda.Literal    (NoLiteral, absurdLiteral)
import           Data.VarName
import Data.Void

---- base types
data LamF v l a x r =
  -- | Function application
    App r r
  -- | Lambda abstraction
  | Abs v (Scope r)
  -- | Let expression
  | Let [(v,r)] (Scope r)
  -- | "let rec" expression
  | LetRec [(v,Scope r)] (Scope r)
  -- | Bound variable
  | BV !Int !Int
  -- | Free variable
  | FV x
  -- | Literal
  | Lit a
  -- | Annotation
  | Loc !l r
  deriving (Show, Eq, Ord, Functor, Foldable, Traversable)

newtype Lam v l a x = Lam { unLam :: LamF v l a x (Lam v l a x) }

newtype Scope x = Scope { unScope :: x }
                deriving (Show, Eq, Ord,
                          Functor, Foldable, Traversable)

---------
app_ :: Lam v l a x -> Lam v l a x -> Lam v l a x
app_ e1 e2 = Lam $ App e1 e2

abs_ :: v -> Scope (Lam v l a x) -> Lam v l a x
abs_ x e = Lam $ Abs x e

let_ :: [(v, Lam v l a x)] -> Scope (Lam v l a x) -> Lam v l a x
let_ defs body = Lam $ Let defs body

letrec_ :: [(v, Scope (Lam v l a x))] -> Scope (Lam v l a x) -> Lam v l a x
letrec_ defs body = Lam $ LetRec defs body

bv_ :: Int -> Int -> Lam v l a x
bv_ !n !m = Lam $ BV n m

fv_ :: x -> Lam v l a x
fv_ x = Lam $ FV x

lit_ :: a -> Lam v l a x
lit_ a = Lam $ Lit a

loc_ :: l -> Lam v l a x -> Lam v l a x
loc_ l e = Lam $ Loc l e

---------
(|$|) :: Lam v l a x -> Lam v l a x -> Lam v l a x
(|$|) = app_
infixl 8 |$|

makeFV :: (IsName x) => String -> Lam v l a x
makeFV = fv_ . fromString

makeAbs :: (Eq v) => v -> Lam v l a v -> Lam v l a v
makeAbs = makeAbs' (==)

makeAbs' :: (v -> x -> Bool) -> v -> Lam v l a x -> Lam v l a x
makeAbs' is v e = abs_ v (abstract' is [v] e)

makeLet :: (Eq v) => [(v, Lam v l a v)] -> Lam v l a v -> Lam v l a v
makeLet = makeLet' (==)

makeLet' :: (v -> x -> Bool) -> [(v, Lam v l a x)] -> Lam v l a x -> Lam v l a x
makeLet' is defs body = let_ defs (wrap body)
  where
    wrap = abstract' is (fmap fst defs)

makeLetRec :: (Eq v) => [(v, Lam v l a v)] -> Lam v l a v -> Lam v l a v
makeLetRec = makeLetRec' (==)

makeLetRec' :: (v -> x -> Bool) -> [(v, Lam v l a x)] -> Lam v l a x -> Lam v l a x
makeLetRec' is defs body = letrec_ (fmap (fmap wrap) defs) (wrap body)
  where
    wrap = abstract' is (fmap fst defs)

-- * Type-changing maps

mapBoundVarName :: (v -> w) -> Lam v l a x -> Lam w l a x
mapBoundVarName f = hoistFix (_mapV f)

mapLoc :: (l -> l') -> Lam v l a x -> Lam v l' a x
mapLoc f = hoistFix (_mapL f)

mapLit :: (a -> b) -> Lam v l a x -> Lam v l b x
mapLit f = hoistFix (_mapA f)

mapFV :: (x -> x') -> Lam v l a x -> Lam v l a x'
mapFV f = hoistFix (_mapFV f)

-- * Instances
type instance Base (Lam v l a x) = LamF v l a x

instance Recursive (Lam v l a x) where
  project = unLam

instance Corecursive (Lam v l a x) where
  embed = Lam

instance Functor (Lam v l a) where
  fmap f = hoistFix (_mapFV f)

instance Foldable (Lam v l a) where
  foldMap f = go
    where
      go (Lam fe) = case fe of
        App e1 e2          -> go e1 <> go e2
        Abs _ (Scope body) -> go body
        Let defs (Scope body) -> foldMap (go . snd) defs <> go body
        LetRec defs (Scope body) -> foldMap (go . unScope . snd) defs <> go body
        BV _ _   -> mempty
        FV v     -> f v
        Lit _    -> mempty
        Loc _ e  -> go e

instance Traversable (Lam v l a) where
  traverse f = cata (fmap Lam . either sequenceA (fmap FV . f) . viewFV)

instance Applicative (Lam v l a) where
  pure = fv_
  (<*>) = ap

instance Monad (Lam v l a) where
  t >>= k = walkFV (\d a -> shift d (k a)) t

instance Bifunctor (Lam v l) where
  bimap f g = hoistFix (_mapA f . _mapFV g)
  first f = hoistFix (_mapA f)
  second g = hoistFix (_mapFV g)

instance Bifoldable (Lam v l) where
  bifoldMap f g = go
    where
      go (Lam fe) = case fe of
        App e1 e2          -> go e1 <> go e2
        Abs _ (Scope body) -> go body
        Let defs (Scope body) -> foldMap (go . snd) defs <> go body
        LetRec defs (Scope body) -> foldMap (go . unScope . snd) defs <> go body
        BV _ _   -> mempty
        FV v     -> g v
        Lit a    -> f a
        Loc _ e  -> go e

instance Bitraversable (Lam v l) where
  bitraverse f g = cata $ \fe -> case viewFV fe of
    Left fe' -> case viewLit fe' of
      Left fe'' -> Lam <$> sequenceA fe''
      Right a   -> Lam . Lit <$> f a
    Right x     -> Lam . FV <$> g x

---- A trick to define Show instance for Lam

---- Never export this type! ----
newtype Showable = Showable { runShowable :: Int -> ShowS }

toShowable :: (Show a) => a -> Showable
toShowable a = Showable (`showsPrec` a)

instance Show Showable where
  showsPrec = flip runShowable

instance (Show v, Show a, Show l, Show x) =>
         Show (Lam v l a x) where
  showsPrec d (Lam e) = showParen (d >= 11) content
    where content = ("Lam "++) . showsPrec 11 (fmap toShowable e)

instance (Eq l, Eq v) => Eq2 (Lam v l) where
  liftEq2 eqA eqFV = eq
    where
      eq s t = go' (unLam s) (unLam t)
      eqScope s t = eq (unScope s) (unScope t)
      go' (App s1 s2) (App t1 t2) = eq s1 t1 && eq s2 t2
      go' (Abs v body) (Abs v' body') = v == v' && eqScope body body'
      go' (Let defs body) (Let defs' body') =
        liftEq (liftEq2 (==) eq) defs defs' && eqScope body body'
      go' (LetRec defs body) (LetRec defs' body') =
        liftEq (liftEq2 (==) eqScope) defs defs' && eqScope body body'
      go' (BV n m) (BV n' m') = n == n' && m == m'
      go' (FV x) (FV x') = eqFV x x'
      go' (Lit a) (Lit a') = eqA a a'
      go' (Loc l e) (Loc l' e') = l == l' && eq e e'
      go' _ _ = False

instance (Eq v, Eq l, Eq a) => Eq1 (Lam v l a) where
  liftEq = liftEq2 (==)

instance (Eq v, Eq l, Eq a, Eq x) => Eq (Lam v l a x) where
  (==) = eq2

-- * "pure" Lambda calculus
type Lam' v x = Lam v Void NoLiteral x

-- | A "pure" (Lam' v) term can be embedded to a type of terms with
--   any literal/extension.
pureToAny :: Lam' v x -> Lam v l a x
pureToAny = hoistFix (_mapL absurd . _mapA absurdLiteral)

anyLiteral :: Lam v l NoLiteral x -> Lam v l a x
anyLiteral = first absurdLiteral

anyLoc :: Lam v Void a x -> Lam v l a x
anyLoc = removeLoc

-- | Remove all metadata.
removeLoc :: Lam v l a x -> Lam v l' a x
removeLoc = cata $ \fe -> case viewLoc fe of
  Left fe' -> Lam fe'
  Right (_, x) -> x

-- | Fold lambda term with innermost metadata surrounding it.
foldWithLoc :: (m -> LamF v m a x r -> r) -> Lam v m a x -> m -> r
foldWithLoc step = cata $ \fe m -> case fe of
  Loc m' r  -> step m $ Loc m' (r m')
  _         -> step m $ fmap ($ m) fe

-- | Traverse free variables with 
traverseFVsWithLoc ::
  (Applicative g) => (m -> x -> g x) -> Lam v m a x
  -> m -> g (Lam v m a x)
traverseFVsWithLoc f = foldWithLoc $ \m fe -> case fe of
  FV x        -> fv_ <$> f m x
  _           -> Lam <$> sequenceA fe

-- | List free variable occurences with innermost metadata surrounding it.
freeVarsWithLoc :: Lam v m a x -> m -> [(m, x)]
freeVarsWithLoc t m0 = foldWithLoc step t m0 []
  where
    step m fe = case fe of
      FV x -> ((m, x):)
      _    -> foldr (.) id fe

-- * Variable name handling

-- | Set of free variables
freeVars :: (Ord x) => Lam v l a x -> Set.Set x
freeVars = foldMap Set.singleton

-- | replace each occurence of free variable with expression
replaceFV :: (Eq x) => x -> Lam v l a x -> Lam v l a x -> Lam v l a x
replaceFV x t = (>>= (\y -> if x == y then t else fv_ y))

-- | Count occurence of free variable @x@.
countFV :: (Eq x) => x -> Lam v l a x -> Int
countFV x = getSum . foldMap f
  where f y = if x == y then Sum 1 else Sum 0

-- * primitives

-- | Replace every bounded variable @BV n i@ with expression @f d n i@,
--   where f is given function, d is abstraction depth.
walk :: (Int -> Int -> Int -> Lam v l a x) -> Lam v l a x -> Lam v l a x
walk f = foldWithDepth $ \d fe ->
  case fe of
    BV n i -> f d n i
    _      -> Lam fe

walkFV :: (Int -> x -> Lam v l a y) -> Lam v l a x -> Lam v l a y
walkFV f = foldWithDepth $ \d fe ->
  case viewFV fe of
    Right x  -> f d x
    Left fe' -> Lam fe'

walkLit :: (Int -> a -> Lam v l b x) -> Lam v l a x -> Lam v l b x
walkLit f = foldWithDepth $ \d fe ->
  case viewLit fe of
    Right a  -> f d a
    Left fe' -> Lam fe'

walkLoc :: (Int -> l -> Lam v m a x -> Lam v m a x) ->
                          Lam v l a x -> Lam v m a x
walkLoc f = foldWithDepth $ \d fe ->
  case viewLoc fe of
    Right (l,e) -> f d l e
    Left fe'  -> Lam fe'

-- | Walk every syntactic node with its abstraction depth.
foldWithDepth :: (Int -> LamF v l a x r -> r) -> Lam v l a x -> r
foldWithDepth f t = cata (\fe d -> f d (step fe d)) t 0
  where
    step fe d = case fe of
      App e1 e2          -> App (e1 d) (e2 d)
      Abs x (Scope body) -> Abs x (Scope (body $! (d+1)))
      Let defs (Scope body) ->
        let !d' = d+1
            newDefs = (fmap . second) ($ d) defs
            newBody = Scope (body d')
        in Let newDefs newBody
      LetRec defs (Scope body) ->
        let !d' = d+1
            newDefs = (fmap . second . fmap) ($ d') defs
            newBody = Scope (body d')
        in LetRec newDefs newBody
      BV n i   -> BV n i
      FV v     -> FV v
      Lit a    -> Lit a
      Loc l e  -> Loc l (e d)

-- | shift (+x) unbound de Bruijn variable
shift :: Int -> Lam v l a x -> Lam v l a x
shift x = walk step
  where step d n m | n >= d    = bv_ (n + x) m
                   | otherwise = bv_ n m

instantiate :: Scope (Lam v l a x) -> [Lam v l a x] -> Lam v l a x
instantiate (Scope s) ts = walk substAux s
  where substAux d n m = case n `compare` d of
          GT -> bv_ (n-1) m
          EQ -> shift d (ts !! m)
          LT -> bv_ n m

abstract' :: (v -> x -> Bool) -> [v] -> Lam w l a x -> Scope (Lam w l a x)
abstract' is xs e = Scope $ foldWithDepth step e
  where
    step d fe = case fe of
      FV y -> case findIndex (`is` y) xs of
        Just m  -> bv_ d m
        Nothing -> fv_ y
      BV n m | n >= d    -> bv_ (n+1) m
             | otherwise -> bv_ n m
      _ -> Lam fe

abstract :: (Eq v) => [v] -> Lam w l a v -> Scope (Lam w l a v)
abstract = abstract' (==)

-- * Type puzzling
viewFV :: LamF v l a x r -> Either (LamF v l a x' r) x
viewFV fx = case fx of
  App x y          -> Left $ App x y
  Abs v x          -> Left $ Abs v x
  Let defs body    -> Left $ Let defs body
  LetRec defs body -> Left $ LetRec defs body
  FV x             -> Right x
  BV m n           -> Left $ BV m n
  Lit a            -> Left $ Lit a
  Loc l e          -> Left $ Loc l e

viewLit :: LamF v l a x r -> Either (LamF v l b x r) a
viewLit fx = case fx of
  App x y          -> Left $ App x y
  Abs v x          -> Left $ Abs v x
  Let defs body    -> Left $ Let defs body
  LetRec defs body -> Left $ LetRec defs body
  FV x             -> Left $ FV x
  BV m n           -> Left $ BV m n
  Lit a            -> Right a
  Loc l e          -> Left $ Loc l e

viewLoc :: LamF v l a x r -> Either (LamF v g a x r) (l, r)
viewLoc fx = case fx of
  App x y          -> Left $ App x y
  Abs v x          -> Left $ Abs v x
  Let defs body    -> Left $ Let defs body
  LetRec defs body -> Left $ LetRec defs body
  FV x             -> Left $ FV x
  BV m n           -> Left $ BV m n
  Lit a            -> Left $ Lit a
  Loc l e          -> Right (l, e)

_mapV :: (v -> v') -> LamF v l a x r -> LamF v' l a x r
_mapV h fx = case fx of
  App x y          -> App x y
  Abs v x          -> Abs (h v) x
  Let defs body    -> Let ((fmap . first) h defs) body
  LetRec defs body -> LetRec ((fmap . first) h defs) body
  BV m n           -> BV m n
  FV x             -> FV x
  Lit a            -> Lit a
  Loc l e          -> Loc l e

_mapFV :: (x -> x') -> LamF v l a x r -> LamF v l a x' r
_mapFV f = either id (FV . f) . viewFV

_mapA :: (a -> b) -> LamF v l a x r -> LamF v l b x r
_mapA h = either id (Lit . h) . viewLit

_mapL :: (l -> l') -> LamF v l a x r -> LamF v l' a x r
_mapL h = either id (\(l,r) -> Loc (h l) r) . viewLoc

-- * Utility
hoistFix :: (Recursive t, Corecursive u) => (Base t u -> Base u u) -> t -> u
hoistFix f = cata (embed . f)
