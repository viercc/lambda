{-# LANGUAGE DeriveTraversable #-}
module BooleanAlgebra(BoolExpr(..), implies, (==>), isTautologyExpr, findCounterexample) where

import Control.Applicative ((<|>))

data BoolExpr x = BoolVar x | BoolCon !Bool | Or (BoolExpr x) (BoolExpr x) | Not (BoolExpr x)
    deriving (Show, Eq)

infixr 2 `implies`
infixr 2 ==>
implies, (==>) :: BoolExpr x -> BoolExpr x -> BoolExpr x
implies p q = Or (Not p) q
(==>) = implies

simplify :: BoolExpr x -> BoolExpr x
simplify (Or p q) = case simplify p of
    BoolCon True -> BoolCon True
    BoolCon False -> simplify q
    p' -> case simplify q of
        BoolCon True -> BoolCon True
        BoolCon False -> p'
        q' -> Or p' q'
simplify (Not p) = case simplify p of
    BoolCon b -> BoolCon (not b)
    Not p''    -> p''
    p' -> Not p'
simplify p = p

setVar :: (Eq x) => x -> Bool -> BoolExpr x -> BoolExpr x
setVar x c = simplify . go
  where
    go (BoolVar y) | x == y = BoolCon c
                   | otherwise = BoolVar y
    go (BoolCon b) = BoolCon b
    go (Or p q) = Or (go p) (go q)
    go (Not p) = Not (go p)

findVarOrEval :: Eq x => BoolExpr x -> Either x Bool
findVarOrEval (BoolVar x) = Left x
findVarOrEval (BoolCon b) = Right b
findVarOrEval (Or p q) = case findVarOrEval p of
    Left x -> Left x
    Right True -> Right True
    Right False -> findVarOrEval q
findVarOrEval (Not p) = not <$> findVarOrEval p

isTautologyExpr :: (Eq x) => BoolExpr x -> Bool
isTautologyExpr p = case findVarOrEval p of
    Left x -> isTautologyExpr (setVar x False p) && isTautologyExpr (setVar x True p)
    Right b -> b

findCounterexample :: (Eq x) => BoolExpr x -> Maybe [(x,Bool)]
findCounterexample = go []
  where
    go vars p = case findVarOrEval p of
        Left x ->
            let p0 = setVar x False p
                p1 = setVar x True p
            in go ((x,False) : vars) p0 <|> go ((x,True) : vars) p1
        Right b -> if b then Nothing else Just vars