{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Lambda.RandomGen(
  genLam,
  genBV, genLit, genFV,
  Env(),
) where

import           Data.String

import           Control.Monad (join)
import           Control.Monad.State

import           Data.Semigroup

import           Test.QuickCheck

import           Data.Lambda.Core
import           Data.Lambda.Util (foldMapBVWithDepth)
import           Data.Lambda.Literal
import           Data.Lambda.Module.Core

import           Once

import Data.List.Infinite (Infinite(..))
import qualified Data.List.Infinite as Inf

instance Arbitrary VarName where
  arbitrary =
    do i <- choose (1, 8 :: Int)
       return (fromString ("fv" ++ show i))

instance (Arbitrary v) => Arbitrary (Qualified v) where
  arbitrary = frequency quals <*> arbitrary
    where
      quals =
        [(1, return (Qualified m)) | m <- ["A", "B", "C"]] ++
        [(4, return Unqualified)]

instance (IsName v) =>
         Arbitrary (Lam v l a x) where
  arbitrary = genLam genBV
  shrink t = subtermsLam t ++ downLam t

instance Arbitrary Literal where
  arbitrary = oneof [ IntegerLit <$> arbitrary
                    , StringLit <$> arbitrary
                    , CharLit <$> arbitrary
                    ]

-- Distribute n to k items. n >= 0, k >= 1.
distribute :: Int -> Int -> Gen [Int]
distribute n k
  | k <= 1 = return [n]
  | n <= 0 = return (replicate k 0)
  | otherwise =
      do n' <- choose (0, n)
         k' <- choose (1, k-1)
         xs1 <- distribute n' k'
         xs2 <- distribute (n-n') (k-k')
         return (xs1 ++ xs2)

sqrti :: Int -> Int
sqrti x | x < 0 = error $ "sqrti(" ++ show x ++ ")"
        | otherwise = floor (sqrt (fromIntegral x) :: Double)

frequency' :: [(Int, a)] -> Gen a
frequency' = frequency . fmap (fmap return)

liftedFrequency :: (MonadTrans t, Monad (t Gen)) =>
  [(Int, t Gen a)] -> t Gen a
liftedFrequency = join . lift . frequency'

resizeSG :: Int -> StateT s Gen a ->  StateT s Gen a
resizeSG n (StateT f) = StateT $ \s -> resize n (f s)

newtype Env = Env [Int]

genBV :: Env -> Gen (Lam v f a x)
genBV (Env env) =
  do (d,i) <- elements
       [ (d,i) | (d,n) <- zip [0..] env, i <- [0..n-1] ]
     return (bv_ d i)

genLit :: (Arbitrary a) => Gen (Lam v f a x)
genLit = lit_ <$> arbitrary

genFV :: (Arbitrary x) => Gen (Lam v f a x)
genFV = fv_ <$> arbitrary

genLam :: (IsName v) =>
  (Env -> Gen (Lam v f a x)) -> Gen (Lam v f a x)
genLam genLeaf = evalStateT (go (Env [])) uniqueNames where
  go env = lift getSize >>= \n ->
    let totalUnits = 1000000
        probLeaf | nullEnv env = 0
                 | otherwise   = totalUnits `div` n
        appWeight | nullEnv env = 0
                  | otherwise   = 40
        totalWeight = appWeight + 60
        probApp = (totalUnits - probLeaf) * appWeight `div` totalWeight
        probAbs = (totalUnits - probLeaf) * 50 `div` totalWeight
        probLet = (totalUnits - probLeaf) * 10 `div` totalWeight
    in liftedFrequency
         [ (probLeaf, lift $ genLeaf env)
         , (probApp, genApp env)
         , (probAbs, genAbs env)
         , (probLet, genLet env) ]
  genApp env =
    do (i, j) <- lift splitSize
       e1 <- resizeSG i $ go env
       e2 <- resizeSG j $ go env
       return (app_ e1 e2)
  genAbs env =
    do n <- max 2 <$> lift getSize
       v <- fresh
       body <- resizeSG (n-1) $ go (consEnv 1 env)
       return (abs_ v (Scope body))
  genLet env =
    do (i, j) <- lift $ greaterFst <$> splitSize
       let numDefs = sqrti i
           env' = consEnv numDefs env
       vs <- freshN numDefs
       defSizes <- map (1+) <$> lift (distribute (i - numDefs) numDefs)
       isLetRec <- lift arbitrary
       if isLetRec
         then do rhss <- mapM (\n' -> Scope <$> resizeSG n' (go env')) defSizes
                 body <- resizeSG j (go env')
                 return (letrec_ (zip vs rhss) (Scope body))
         else do rhss <- mapM (\n' -> resizeSG n' (go env)) defSizes
                 body <- resizeSG j (go env')
                 return (let_ (zip vs rhss) (Scope body))

  consEnv :: Int -> Env -> Env
  consEnv n (Env env) = Env (n : env)

  nullEnv :: Env -> Bool
  nullEnv (Env env) = null env
  
  splitSize =
    do n <- max 2 <$> getSize
       i <- choose (1, n-1)
       return (i, n-i)
  
  greaterFst (a, b) | a < b = (b, a)
                    | otherwise = (a, b)
  
  fresh = state (\(v :< ng') -> (v, ng'))
  freshN n = state (Inf.splitAt n)

subtermsLam :: Lam v f a x -> [Lam v f a x]
subtermsLam (Lam fe) =
  case fe of
    App e1 e2        -> [e1, e2]
    Abs _ body       -> unwrap body
    Let defs body    -> fmap snd defs ++ unwrap body
    LetRec defs body -> concatMap (unwrap . snd) defs ++ unwrap body
    Loc _ e          -> [e]
    BV _ _           -> []
    FV _             -> []
    Lit _            -> []
  where
    isConst = getAll . foldMapBVWithDepth (\d n _ -> All (n /= d))
    unwrap (Scope t) = [shift (-1) t | isConst t]

downLam :: (IsName v) =>
  Lam v f a x -> [Lam v f a x]
downLam (Lam fe) =
  case fe of
    FV _ -> []
    BV _ _ -> []
    Lit _ -> []
    _ -> Lam <$> shrinkTrav shrink fe
