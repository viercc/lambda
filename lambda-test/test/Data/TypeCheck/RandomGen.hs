{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.TypeCheck.RandomGen(
  genType,
  shrinkType,
  isTautology,
  isEasy
) where

import qualified Data.Set as Set

import           Test.QuickCheck

import           Data.TypeCheck.Core

import           Data.Lambda.RandomGen()
import           Once
import           BooleanAlgebra

-------------------

instance Arbitrary Type where
  arbitrary = genType defaultLeafTypes [("List", 1)]
  shrink = shrinkType

instance Arbitrary TypeScheme where
  arbitrary = mkTS <$> genType defaultLeafTypes [("List", 1)]
    where
      mkTS t = TypeScheme (Set.toList (freeVars t)) t
  shrink (TypeScheme vs t) = TypeScheme vs <$> shrink t

defaultLeafTypes :: [Type]
defaultLeafTypes =
  map TV ["a","b","c"]

genType :: [Type] -> [(String, Int)] -> Gen Type
genType leafTypes tyCons =
  sized $ \n -> choose (1,n) >>= genTypeN
  where
    genTypeN n | n <= 1    = elements leafTypes
               | otherwise = frequency [(2, genArr n), (1, genTyCon n)]

    genArr n =
      do ns <- fmap (+1) <$> distribute (n-3) 2
         TC "->" <$> traverse genTypeN ns
    genTyCon n =
      do let kmax = sqrti n
             limitedTyCons = filter ((<=kmax) . snd) tyCons
         if null limitedTyCons
           then genArr n
           else do (tc, k) <- elements limitedTyCons
                   ns <- fmap (+1) <$> distribute (n-k-1) k
                   TC tc <$> traverse genTypeN ns

isTautology :: Type -> Bool
isTautology = (==Just True) . checkClassicalTautology

checkClassicalTautology :: Type -> Maybe Bool
checkClassicalTautology t = isTautologyExpr <$> convert t
  where
    convert (TV a) = Just $ BoolVar a
    convert (TC "->" [a,b]) = implies <$> convert a <*> convert b
    convert (TC _ []) = Just $ BoolCon True
    convert _ = Nothing

isEasy :: Type -> Bool
isEasy = loop []
  where
    loop _  (TC _ [])        = True
    loop ts t | t `elem` ts = True
    loop ts (TC "->" [x,y]) = loop (x:ts) y
    loop _   _               = False

shrinkType :: Type -> [Type]
shrinkType (TV _) = []
shrinkType (TC tc subs) =
  subs ++ (TC tc <$> shrinkTrav shrinkType subs)

--------------------------

-- Distribute n to k items. n >= 0, k >= 1.
distribute :: Int -> Int -> Gen [Int]
distribute n k
  | k <= 1 = return [n]
  | n <= 0 = return (replicate k 0)
  | otherwise =
      do n' <- choose (0, n)
         k' <- choose (1, k-1)
         xs1 <- distribute n' k'
         xs2 <- distribute (n-n') (k-k')
         return (xs1 ++ xs2)

sqrti :: Int -> Int
sqrti x | x < 0 = error $ "sqrti(" ++ show x ++ ")"
        | otherwise = floor (sqrt (fromIntegral x) :: Double)
