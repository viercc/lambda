{-# LANGUAGE DeriveTraversable #-}
module Once where

shrinkTrav :: (Traversable f) => (a -> [a]) -> f a -> [f a]
shrinkTrav shrink' = getVariants . traverse (\a -> Once a (shrink' a))

data Once a = Once { getDefault :: a, getVariants :: [a] }
  deriving (Show, Read, Eq, Ord, Functor, Foldable, Traversable)

instance Applicative Once where
  pure a = Once a []
  Once f fs <*> Once x xs =
    Once (f x) (fmap ($ x) fs ++ fmap f xs)

instance Monad Once where
  return = pure
  Once a as >>= k =
    let Once b bs = k a
        bs' = fmap (getDefault . k) as
    in Once b (bs' ++ bs)
