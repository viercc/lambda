{-# LANGUAGE OverloadedStrings #-}
module Spec.ProofSearchSpec(spec) where

import Test.Hspec
import Test.QuickCheck as QC
import Test.QuickCheck.Property as QCP

import Spec.Common

import qualified Data.Map.Lazy as Map
import qualified Data.Set as Set

import Data.VarName
import Data.Lambda.Core (Lam)
import Data.Lambda.TypeCheck
import Data.Lambda.ProofSearch as Lam
import Data.Lambda.Literal
import Data.Lambda.Parse

import Data.Void

import Data.TypeCheck.Core
import Data.TypeCheck.TypeInfer

import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

import Data.TypeCheck.RandomGen

type L0 = Lam VarName Void Void VarName

spec :: Spec
spec = do
  generatesWell "id"       8 [] idT
  generatesWell "const"    8 [] constT
  generatesWell "ap"       12 [] apT
  generatesWell "tree"     14 [] treeT
  generatesWell "compose3" 12 [] compose3T
  generatesWell "fn"       12 intEnv fnT
  generatesNothing "bad1" 12 [] bad1T
  generatesNothing "bad2" 12 [] bad2T
  it "For random types" $ withMaxSuccess 100 $ property $
    forAll (genTSAndLam 12 10) $ \(ts, exprs) ->
      conjoin [ hasType expr [] ts | expr <- exprs ]

hasType :: L -> [(VarName, TypeScheme)] -> TypeScheme -> Property
hasType expr env ts = property $
  case tyAgainst env expr ts of
    Good _ -> succeeded
    Bad es ->
      let reasonStr = show (pretty es) ++ "\n" ++
                      "in expr: \n" ++ formatL expr ++ "\n" ++
                      "  :: " ++ format ts ++ "\n"
      in failed{ QCP.reason=reasonStr }

genTS :: Gen TypeScheme
genTS =
  let leaves = map TV ["a","b","c"]
      g = resize 10 (genType leaves []) `suchThat` (\t -> isTautology t && not (isEasy t))
      postprocess t = TypeScheme (Set.toList (freeVars t)) t
  in postprocess <$> g

genTSAndLam :: Int -> Int -> Gen (TypeScheme, [Lam VarName f a VarName])
genTSAndLam size maxCount = gen' `suchThatMap` id
  where
    gen' = do
      ts <- genTS
      (lams, _) <- randomDrawBy chooseAny maxCount (Lam.generate size [] ts)
      case lams of
        [] -> pure Nothing
        _ -> pure $ Just (ts, lams)

generatesWell :: String -> Int -> [(VarName, TypeScheme)] -> TypeScheme -> Spec
generatesWell name size env targetType =
  let genExprs num = fst <$> randomDrawBy chooseAny num (Lam.generate size env targetType)
      itemStr = name ++ " :: " ++ format targetType
  in it itemStr $ once $
       forAll (genExprs 10) $ \exprs ->
         counterexample "length exprs == 0" (length exprs `shouldNotBe` 0)
           .&&.
         conjoin
           [ counterexample (formatL expr) (hasType expr env targetType)
           | expr <- exprs
           ]

generatesNothing :: String -> Int -> [(VarName, TypeScheme)] -> TypeScheme -> Spec
generatesNothing name size env targetType =
  let genExpr = observeOne $ Lam.generate size env targetType
      itemStr = "[SHOULD BE NONE] " ++ name ++ " :: " ++ format targetType
  in it itemStr $ genExpr `shouldBe` (Nothing :: Maybe L0)

idT, constT, apT, treeT, compose3T, fnT :: TypeScheme
idT = TypeScheme ["a"] (TV "a" ~> TV "a")
constT = TypeScheme ["a", "b"] (TV "a" ~> TV "b" ~> TV "a")
compose3T = TypeScheme ["a", "b", "c", "d"] $
  (TV "c" ~> TV "d") ~>
  (TV "b" ~> TV "c") ~>
  (TV "a" ~> TV "b") ~>
  (TV "a" ~> TV "d")
apT = TypeScheme ["a", "b", "c"] $
  (TV "a" ~> TV "b" ~> TV "c") ~>
  (TV "a" ~> TV "b") ~>
  TV "a" ~> TV "c"
treeT = TypeScheme ["a", "t"] $
  (TV "a" ~> TV "t") ~>
  (TV "t" ~> TV "t" ~> TV "t") ~>
  TV "a" ~> TV "a" ~> TV "t"
fnT = mono (intT ~> intT)
intT :: Type
intT = TC "Int" []

bad1T, bad2T :: TypeScheme
bad1T = TypeScheme ["a", "b"] $ TV "a" ~> TV "b"
bad2T = TypeScheme ["a", "b"] $ ((TV "a" ~> TV "b") ~> TV "a") ~> TV "a"

mono :: Type -> TypeScheme
mono = TypeScheme []

intEnv :: [(VarName, TypeScheme)]
intEnv = [ ("plus", mono (intT ~> intT ~> intT)) ]

tyAgainst :: [(VarName, TypeScheme)] -> L -> TypeScheme -> TCR
tyAgainst env e tscheme =
     fmap fst $
     runTypeInfer (BuiltinPos "top") $
     inferTypeAnnot (typeLam withPos typeCheckLit typeCheckFV e) tscheme
  where
    envMap = Map.fromList env
    typeCheckFV x = case Map.lookup x envMap of
      Just ts -> do (_, t) <- instantiate ts
                    return (t, emptySubst)
      Nothing -> undefinedVar x

typeCheckLit :: Literal -> TI SourceRange VarName
typeCheckLit lit = case lit of
  IntegerLit _ -> return (TC "Int" [], emptySubst)
  StringLit _  -> return (TC "String" [], emptySubst)
  CharLit _    -> return (TC "Char" [], emptySubst)
