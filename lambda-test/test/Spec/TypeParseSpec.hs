{-# LANGUAGE OverloadedStrings #-}
module Spec.TypeParseSpec(spec) where

import Data.Foldable (for_)

import Test.Hspec
import Test.QuickCheck

import Data.TypeCheck.Core
import Data.TypeCheck.Parse
import Data.TypeCheck.RandomGen()

import Spec.Common (failureDiffer)

import Text.PrettyPrint.Formattable

spec :: Spec
spec = do
  for_ tests $ \(tsStr,ts) ->
    it ("Should parse " ++ show tsStr) $
      shouldParseTS tsStr ts
  it "parse . format = id" $
    property $ \ts ->
      parseTypeScheme (format (ts :: TypeScheme)) === Right ts

tests :: [(String, TypeScheme)]
tests =
  [ ("forall a. a -> a", TypeScheme ["a"] (TC "->" [TV "a", TV "a"]))
  , ("forall a. List a", TypeScheme ["a"] (TC "List" [TV "a"]))
  , ("Bool", TypeScheme [] (TC "Bool" []))
  , ("forall a. List a -> List Bool",
       TypeScheme ["a"] $
         TC "->" [ TC "List" [TV "a"]
                 , TC "List" [TC "Bool" []]
                 ])
  ]

shouldParseTS :: String -> TypeScheme -> Expectation
shouldParseTS tsStr ts =
  case parseTypeScheme tsStr of
    Left err  -> expectationFailure err
    Right ts' ->
      if equivTypeScheme ts ts'
        then return ()
        else failureDiffer (formatDoc ts) (formatDoc ts')
