module Spec.ModuleParseSpec(spec) where

import           Test.Hspec

import           System.Directory

import           Control.Monad

import           Data.Either.Validation
import           Data.Void

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Data.VarName

import           Data.Lambda.Format           ()
import           Data.Lambda.Module.Core
import           Data.Lambda.Module.Format
import           Data.Lambda.Module.Loader
import           Data.Lambda.Module.Parse

import           Data.TypeCheck.Core          (TypeScheme)
import           Data.TypeCheck.Parse         (typeSchemeP)

spec :: Spec
spec = do
  forM_ ["M001", "M002", "M003", "M004"] $ \modName ->
    it ("Should parse: " ++ show (modName ++ ".lam")) $
      shouldBeSuccess $ parseModule modName
  forM_ ["DoNotExist", "T001", "B001", "B002", "B003", "B004"] $ \modName ->
    it ("Should not parse: " ++ show (modName ++ ".lam")) $
      shouldBeFailure $ fmap formatDocModule <$> parseModule modName
  forM_ ["M001", "T001", "T002"] $ \modName ->
    it ("Should parse (with Type): " ++ show (modName ++ ".lam")) $
      shouldBeSuccess $ parseModuleT modName
  forM_ ["BT001", "BT002"] $ \modName ->
    it ("Should not parse (with Type): " ++ show (modName ++ ".lam")) $
      shouldBeFailure $ fmap formatDocModule <$> parseModuleT modName

parseModule :: ModuleName -> Load VarName IO (ParsedModule Void Void)
parseModule modName =
  withCurrentDirectory "./testdata/module" $
    parseModuleFileIO (moduleP mzero mzero) "lam" modName

parseModuleT :: ModuleName -> Load VarName IO (ParsedModule TypeScheme Void)
parseModuleT modName =
  withCurrentDirectory "./testdata/module" $
    parseModuleFileIO (moduleP typeSchemeP mzero) "lam" modName

shouldBeSuccess :: (Pretty e) => IO (Validation e a) -> Expectation
shouldBeSuccess mr =
  mr >>= \r -> case r of
    Success _ -> return ()
    Failure e -> expectationFailure $ show (pretty e)

shouldBeFailure :: (Formattable a) => IO (Validation e a) -> Expectation
shouldBeFailure mr =
  mr >>= \r -> case r of
    Success a -> expectationFailure $ format a
    Failure _ -> return ()
