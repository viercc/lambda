{-# LANGUAGE QuasiQuotes #-}
module Spec.AlphaEqvSpec(spec) where

import           Test.Hspec

import           Data.Lambda.Parse     (ParsedLam)
import           Data.Lambda.TH.Simple

import qualified Spec.Common

(===), (=/=) :: ParsedLam () -> ParsedLam () -> Expectation
(===) = (Spec.Common.===)
(=/=) = (Spec.Common.=/=)

spec :: Spec
spec = do
  it "test1" $
    [lambda| \x y -> x y x |] ===
    [lambda| \a b -> a b a |]
  it "test2" $
    [lambda| \x y -> x y x |] =/=
    [lambda| \a b -> a (b a) |]
  it "test3" $
    [lambda| \a -> let { x = a; y = \b -> b } in x a y |] ===
    [lambda| \b -> let { z = b; w = \c -> c } in z b w |]
  it "test4" $
    [lambda| let { x = \a -> a; y = \a b -> b } in x y |] ===
    [lambda| let { y = \c d -> d; x = \c -> c } in x y |]
  it "test5" $
    [lambda| let { x = \a -> a; y = \a b -> b } in x y |] =/=
    [lambda| let { x = \c d -> d; y = \c -> c } in x y |]
  it "test6" $
    [lambda| let { x = \a -> a; y = \a b -> b } in x y |] =/=
    [lambda| letrec { y = \c d -> d; x = \c -> c } in x y |]
  it "test7" $
    [lambda| letrec { x = \f -> f y y; y = \g -> g x } in x |] ===
    [lambda| letrec { z = \f -> f w w; w = \g -> g z } in z |]
  it "test8" $
    [lambda| let { r = let { x1 = \a -> a; x2 = \a b -> a } in x1 x2 } in r |] ===
    [lambda| let { r = let { y2 = \a b -> a; y1 = \a -> a } in y1 y2 } in r |]
