{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE PartialTypeSignatures #-}
module Spec.Common(  
  L,
  formatL, formatDocL,
  (===), (=/=),
  shouldParsesTo, shouldNotParse,

  lambdaLit,

  equivLam,
  shouldEquiv, shouldNotEquiv,

  TCR, ty, shouldTypeInferTo,

  failureDiffer,
) where

import           Test.Hspec

import           Control.Monad.ST
import qualified Data.Set                         as Set

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Data.VarName

import           Data.Lambda
import qualified Data.Lambda.Core                 as Lam
import           Data.Lambda.Equivalence
import           Data.Lambda.Format               (formatLamP)
import           Data.Lambda.LazyEval
import           Data.Lambda.LazyEval.Equivalence
import           Data.Lambda.Literal
import           Data.Lambda.Parse
import           Data.Lambda.TH.Simple
import           Data.Lambda.TypeCheck

import           Data.TypeCheck
import           Data.TypeCheck.Core              (equivTypeScheme)

-- * Lam
type L = ParsedLam Literal

formatL :: L -> String
formatL = formatLam

formatDocL :: L -> Doc ann
formatDocL = formatLamP 0

(===) :: (Eq a, Formattable a) => ParsedLam a -> ParsedLam a -> Expectation
t === u
  | t `alphaEquiv` u = return ()
  | otherwise        = failureDiffer (formatLamP 0 t) (formatLamP 0 u)

(=/=) :: (Eq a, Formattable a) => ParsedLam a -> ParsedLam a -> Expectation
t =/= u
  | t `alphaEquiv` u = failureDiffer (formatLamP 0 t) (formatLamP 0 u)
  | otherwise        = return ()

-- * Parsing
shouldParsesTo :: String -> L -> Expectation
shouldParsesTo src t =
  case parse' (lamP literalP) src of
    Left err -> expectationFailure err
    Right s  -> s === t

shouldNotParse :: String -> Expectation
shouldNotParse src =
  case parse' (lamP literalP) src of
    Left _  -> return ()
    Right _ -> expectationFailure $ "Should Not Parse:\n" ++ src

-- * QuasiQuote
lambdaLit :: QuasiQuoter
lambdaLit = qqLam literalP

-- * Operational Equivalence
myEval :: ParsedLam a -> Val () s (Either a VarName)
myEval e = evalStepping () (con . Left) (con . Right) (Lam.removeLoc e) >>= forceValue
  where
    con a = ConHead' a []

equivLam :: (Eq a) => ParsedLam a -> ParsedLam a -> Maybe Bool
equivLam t1 t2 = runST (equiv 1000 1000 constants (myEval t1) (myEval t2))
  where
    fvs = Set.union (Lam.freeVars t1) (Lam.freeVars t2)
    constants = Right <$> uniqueNamesSans fvs

shouldEquiv, shouldNotEquiv :: (Eq a, Formattable a) => ParsedLam a -> ParsedLam a -> Expectation
shouldEquiv t1 t2 =
  case equivLam t1 t2 of
    Just True  -> return ()
    Just False -> Spec.Common.failureDiffer (formatLamP 0 t1) (formatLamP 0 t2)
    Nothing    -> return ()
shouldNotEquiv t1 t2 =
  case equivLam t1 t2 of
    Just True  -> Spec.Common.failureDiffer (formatLamP 0 t1) (formatLamP 0 t2)
    Just False -> return ()
    Nothing    -> return ()

-- * TypeCheck
type TCR = TypeCheckResult SourceRange VarName TypeScheme

ty :: L -> TCR
ty = fmap (generalize' . fst) .
     runTypeInfer (BuiltinPos "top") .
     typeLam' typeCheckLit
  where
    generalize' t = generalize t envEmpty

typeCheckLit :: Literal -> TI SourceRange VarName
typeCheckLit lit = case lit of
  IntegerLit _ -> return (TC "Int" [], emptySubst)
  StringLit _  -> return (TC "String" [], emptySubst)
  CharLit _    -> return (TC "Char" [], emptySubst)

shouldTypeInferTo :: L -> Maybe TypeScheme -> Expectation
shouldTypeInferTo expr expect =
  let tcr = ty expr
      expectDoc = maybe "Can't type" formatDoc expect
      actualDoc = case tcr of
        Good inferred -> formatDoc inferred
        Bad errors    -> "Can't type:" <> hardline <> indent 2 (pretty errors)
  in case (tcr, expect) of
       (Good inferred, Just expectType)
          | inferred `equivTypeScheme` expectType -> return ()
       (Bad _, Nothing) -> return ()
       _ -> failureDiffer expectDoc actualDoc

-- * Util

failureDiffer :: Doc ann -> Doc ann -> Expectation
failureDiffer actualDoc expectDoc =
  expectationFailure $ show $
    nest 2 ("actual:   " <+> actualDoc) <+> hardline <>
    nest 2 ("expected: " <+> expectDoc)
