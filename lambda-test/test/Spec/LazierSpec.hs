{-# LANGUAGE QuasiQuotes #-}
module Spec.LazierSpec(spec) where

import Test.Hspec

import Spec.Common
import Data.Lambda.TH.Lazier

type LL = Lazier ()

spec :: Spec
spec = do
  it "can use other variable in scope (ripLazier)" $
    ripLazier test2 === ripLazier testUnpacked
  it "can use other variable in scope (packLazier)" $
    packLazier [lazier| testRec1 |] === packLazier testRecUnpacked

test1 :: LL
test1 = [lazier| \x y -> x x (y y) |]

test2 :: LL
test2 = [lazier| test1 (\x -> x) |]

testUnpacked :: LL
testUnpacked = [lazier| (\x y -> x x (y y)) (\x -> x)) |]

testRec1 :: LL
testRec1 = [lazier| (\x y -> x) testRec2 |]

testRec2 :: LL
testRec2 = [lazier| (\x y -> y) testRec1 |]

testRecUnpacked :: LL
testRecUnpacked = [lazier|
  letrec {
    testRec1 = (\x y -> x) testRec2;
    testRec2 = (\x y -> y) testRec1
  } in testRec1
|]
