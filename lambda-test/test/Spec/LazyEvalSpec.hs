{-# LANGUAGE QuasiQuotes #-}
module Spec.LazyEvalSpec(spec) where

import           Test.Hspec

import           Data.Lambda.Parse                (ParsedLam)
import           Data.Lambda.TH.Simple

import qualified Spec.Common

(===), (=/=) :: ParsedLam () -> ParsedLam () -> Expectation
(===) = Spec.Common.shouldEquiv
(=/=) = Spec.Common.shouldNotEquiv

spec :: Spec
spec = do
  it "test1" $
    [lambda| \x y -> x y x |] ===
    [lambda| \a b -> a b a |]
  it "test2" $
    [lambda| \x y -> (x y) x |] =/=
    [lambda| \a b -> a (b a) |]
  it "test3" $
    [lambda| \x y -> x y x |] =/=
    [lambda| \a b -> a b |]
  it "test3" $
    [lambda| \a -> let { x = a; y = \b -> b } in x a y |] ===
    [lambda| \b -> let { w = \c -> c } in b b w |]
  it "test4" $
    [lambda| let { x = \a -> a; y = \a b -> b } in y x |] ===
    [lambda| \d -> d |]
  it "test5" $
    [lambda| let { x = \a -> a; y = \a b -> b } in y x |] =/=
    [lambda| let { x = \c d -> d; y = \c -> c } in y x |]
  it "test6" $
    [lambda| let { x = \a -> a; y = \a b -> b } in x y |] ===
    [lambda| letrec { y = \c d -> d; x = \c -> c } in x y |]
  it "test7" $
    [lambda| letrec { x = \f -> f y y; y = \g -> g x } in y |] ===
    [lambda| letrec { w = \g -> g (\f -> f w w) } in w |]
