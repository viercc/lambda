module Spec.ModuleLoadSpec(spec) where

import           Test.Hspec

import           System.Directory

import           Control.Monad

import           Data.Either.Validation
import           Data.Map.Strict              (Map)
import           Data.Void

import           Text.PrettyPrint.Formattable
import           Text.PrettyPrint.Util

import           Data.VarName

import           Data.Lambda.Format           ()
import           Data.Lambda.Module.Core
import           Data.Lambda.Module.Format
import           Data.Lambda.Module.Loader
import           Data.Lambda.Module.Parse

spec :: Spec
spec = do
  forM_ ["M001", "M002", "M003", "M004", "M005", "M006"] $ \modName ->
    it ("Should load: " ++ show (modName ++ ".lam")) $
      shouldBeSuccess $ loadModules' modName
  forM_ ["B001", "B002", "B003", "B004", "B005", "B006", "B007"] $ \modName ->
    it ("Should fail to load: " ++ show (modName ++ ".lam")) $
      shouldBeFailure $ fmap formatModuleSet <$> loadModules' modName

loadModules' :: ModuleName ->
  Load VarName IO (Map ModuleName (ParsedModule Void Void))
loadModules' modName =
  withCurrentDirectory "./testdata/module" $
    loadModulesIO (moduleP mzero mzero) "lam" modName

formatModuleSet :: Map ModuleName (ParsedModule Void Void) -> Doc ann
formatModuleSet = formatDoc . fmap formatDocModule

shouldBeSuccess :: (Pretty e) => IO (Validation e a) -> Expectation
shouldBeSuccess mr =
  mr >>= \r -> case r of
    Success _ -> return ()
    Failure e -> expectationFailure $ show (pretty e)

shouldBeFailure :: (Formattable a) => IO (Validation e a) -> Expectation
shouldBeFailure mr =
  mr >>= \r -> case r of
    Success a -> expectationFailure $ format a
    Failure _ -> return ()
