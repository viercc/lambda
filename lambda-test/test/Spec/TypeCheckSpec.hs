module Spec.TypeCheckSpec(spec) where

import Control.Monad (forM_)
import Test.Hspec

import Spec.Common
import Spec.Examples

spec :: Spec
spec =
  forM_ examples $ \ex ->
    it (egName ex) $ egLam ex `shouldTypeInferTo` egTy ex
