{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RankNTypes        #-}
module Spec.Examples where

import           Text.Heredoc

import           Data.Lambda.Core
import           Data.Lambda.Util
import           Data.TypeCheck
import           Data.TypeCheck.TH

import           Spec.Common

data Example = Example
  { egName :: String
  , egStr  :: String
  , egLam  :: L
  , egTy   :: Maybe TypeScheme
  }
  deriving (Show)

examples :: [Example]
examples = [
  Example {
    egName = "simple.1"
  , egStr = [here| \x y -> x y y |]
  , egLam = lam2 "x" "y" $ \x y -> x |$| y |$| y
  , egTy = Just [tscheme| forall a b. (a -> a -> b) -> a -> b |]
  },
  Example {
    egName = "simple.2"
  , egStr = [here| \f x -> f (x x) |]
  , egLam = lam2 "f" "x" $ \f x -> f |$| (x |$| x)
  , egTy = Nothing
  },
  Example {
    egName = "simple.3"
  , egStr = [here| (\x y -> x) (\x y -> y) (\x -> x) |]
  , egLam = let k = lam2 "x" "y" const
                l = lam2 "x" "y" (const id)
                i = lam "x" id
            in k |$| l |$| i
  , egTy = Just [tscheme| forall a b. a -> b -> b |]
  },
  Example {
    egName = "simple.4"
  , egStr = [here| \x y z -> y x ((\e -> y) z)  |]
  , egLam = lam3 "x" "y" "z" $
      \x y z -> y |$| x |$| (lam "e" (\_ -> S <$> y) |$| z)
  , egTy = Nothing
  },
  Example {
    egName = "simple.5"
  , egStr = [here| \a b -> a (\e -> (\f -> a f) (\g -> g) b) |]
  , egLam = Lam (Abs "a" (Scope {unScope = Lam (Abs "b" (Scope {unScope = Lam (App (Lam (BV 1 0)) (Lam (Abs "e" (Scope {unScope = Lam (App (Lam (App (Lam (Abs "f" (Scope {unScope = Lam (App (Lam (BV 3 0)) (Lam (BV 0 0)))}))) (Lam (Abs "g" (Scope {unScope = Lam (BV 0 0)}))))) (Lam (BV 1 0)))}))))}))}))
  , egTy = Just [tscheme| forall p t. ((p -> p) -> t -> p) -> t -> t -> p |]
  },
  Example {
    egName = "simple.6"
  , egStr = [here| \a -> (\c e -> e (e c)) (\k -> a) |]
  , egLam = Lam (Abs "a" (Scope {unScope = Lam (App (Lam (Abs "c" (Scope {unScope = Lam (Abs "e" (Scope {unScope = Lam (App (Lam (BV 0 0)) (Lam (App (Lam (BV 0 0)) (Lam (BV 1 0)))))}))}))) (Lam (Abs "k" (Scope {unScope = Lam (BV 1 0)}))))}))
  , egTy = Just [tscheme| forall a b. a -> ((b -> a) -> b -> a) -> b -> a |]
  },
  Example {
    egName = "regress.1"
  , egStr = [here| \a b -> (\c -> (\d -> a) (\e -> e)) ((\f -> a) a) |]
  , egLam = Lam (Abs "a" (Scope {unScope = Lam (Abs "b" (Scope {unScope = Lam (App (Lam (Abs "c" (Scope {unScope = Lam (App (Lam (Abs "d" (Scope {unScope = Lam (BV 3 0)}))) (Lam (Abs "e" (Scope {unScope = Lam (BV 0 0)}))))}))) (Lam (App (Lam (Abs "f" (Scope {unScope = Lam (BV 2 0)}))) (Lam (BV 1 0)))))}))}))
  , egTy = Just [tscheme| forall a b. a -> b -> a |]
  },
  Example {
    egName = "let.1"
  , egStr = [here| let { tw = \g x -> g (g x) } in \f -> tw tw f |]
  , egLam =
      let tw = lam2 "g" "x" $ \g x -> g |$| (g |$| x)
      in makeLet [("tw", tw)] $ lam "f" $ \f ->
           fv_ "tw" |$| fv_ "tw" |$| f
  , egTy = Just [tscheme| forall a. (a -> a) -> a -> a |]
  },
  Example {
    egName = "let.2"
  , egStr = [here| \a ->
      let { b = (\f -> a) a;
            c = a
      } in b c |]
  , egLam = Lam (Abs "a" (Scope {unScope = Lam (Let [("b",Lam (App (Lam (Abs "f" (Scope {unScope = Lam (BV 1 0)}))) (Lam (BV 0 0)))),("c",Lam (BV 0 0))] (Scope {unScope = Lam (App (Lam (BV 0 0)) (Lam (BV 0 1)))}))}))
  , egTy = Nothing
  },
  Example {
    egName = "let.3"
  , egStr = [here| \a b -> let { c = (\f -> a) a; d = b } in b c |]
  , egLam = Lam (Abs "a" (Scope {unScope = Lam (Abs "b" (Scope {unScope = Lam (Let [("c",Lam (App (Lam (Abs "f" (Scope {unScope = Lam (BV 2 0)}))) (Lam (BV 1 0)))),("d",Lam (BV 0 0))] (Scope {unScope = Lam (App (Lam (BV 1 0)) (Lam (BV 0 0)))}))}))}))
  , egTy = Just [tscheme| forall a b. a -> (a -> b) -> b |]
  },
  Example {
    egName = "letrec.1"
  , egStr = [here| \f -> letrec { x = f x } in x |]
  , egLam = lam "f" $ \f ->
      makeLetRec' (succEq (==)) [("x", f |$| fv_ "x" )] (fv_ "x")
  , egTy = Just [tscheme| forall a. (a -> a) -> a |]
  }
  ]
