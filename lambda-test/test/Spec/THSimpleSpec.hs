{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
module Spec.THSimpleSpec(spec) where

import Test.Hspec

import Spec.Common (lambdaLit)
import Data.Lambda.Literal
import Data.Lambda.Parse
import Data.Lambda.TH.Simple

spec :: Spec
spec = it "compiles" True

test1 :: ParsedLam'
test1 = [lambda| let { z = \f x -> f (f x) } in \x y -> x z (y z) |]

test2 :: ParsedLam'
test2 = [lambda| hoge |]

test3 :: ParsedLam Literal
test3 = [lambdaLit|
  let {
    cons = \a b f z -> f a (b f z);
    nil = \f z -> z
  } in cons 1 (cons 2 (cons 3 (cons "4" nil)))
|]
