{-# LANGUAGE QuasiQuotes #-}
module Spec.ManipulationSpec(spec) where

import Data.Foldable (for_)

import           Test.Hspec
import           Test.QuickCheck          hiding ((===), (=/=), reason)
import           Test.QuickCheck.Property hiding ((===), (=/=))

import           Data.Lambda.Rename
import           Data.Lambda.Manipulation
import           Data.Lambda.Manipulation.Simplify
import           Data.Lambda.TH.Simple

import           Spec.Common hiding ((===), (=/=))
import qualified Spec.Common
import           Data.Lambda.RandomGen           ()

import           Data.TypeCheck.Core (TypeScheme)
import           Data.TypeCheck.Unify (isGeneralThan)
import           Data.TypeCheck.TypeInfer (TypeCheckResult(..))

import           Text.PrettyPrint.Formattable
import Data.Lambda.Parse (ParsedLam)
import Data.VarName (VarName)

(===) :: L -> L -> Expectation
(===) = (Spec.Common.===)

(=/=) :: L -> L -> Expectation
(=/=) = (Spec.Common.=/=)

-- | Check given transformation of lambda expression do not change
--   its semantics.
soundConversion :: (L -> L) -> Property
soundConversion f =
  property $ forAllShow arbitrary formatL $ \expr ->
    discardNothing $ Spec.Common.equivLam expr (f expr)

discardNothing :: (Testable prop) => Maybe prop -> Property
discardNothing = maybe (property Discard) property

-- | Check type preservation
typePreservation :: (L -> L) -> Property
typePreservation f =
  property $ forAllShow (resize 30 arbitrary) formatL $ \expr ->
    case (Spec.Common.ty expr, Spec.Common.ty (f expr)) of
      (Good ts, Good ts') -> ts' `isGeneralThan'` ts
      (Good _,  Bad _)    -> property False
      (Bad _, _)          -> property Discard

isGeneralThan' :: TypeScheme -> TypeScheme -> Property
isGeneralThan' ts us =
  case isGeneralThan () ts us of
    Right _ -> property True
    Left _  ->
      let msg = format ts ++ " `isGeneralThan` " ++ format us
      in property $ failed{ reason=msg }

spec :: Spec
spec = do
  context "shrinkBeta" $ do
    it "noChange" $
      shrinkBeta [lambda| \x y -> x y x |] === [lambda| \x y -> x y x |]
    it "phantom" $
      shrinkBeta [lambda| (\x y -> y) (\x y -> y x) |] === [lambda| \y -> y |]
    it "identity" $
      shrinkBeta [lambda| (\x -> x) (\x y -> y x) |] === [lambda| \x y -> y x |]
    it "multiple" $
      shrinkBeta [lambda| (\x y -> y x) (\z -> z) (\w -> w) |] === [lambda| \a -> a |]
    it "complex" $
      shrinkBeta [lambda| \f -> (\s z -> s (s z)) (\z -> f (f z)) |] ===
                 [lambda| \f z -> f (f (f (f z))) |]
    it "don'tExplode" $
      {-
      LHS reduces to RHS, but shrinkBeta should not reduce this because
      it increases size of the term.
      -}
      shrinkBeta [lambda| (\s z -> s (s (s (s z)))) (\s z -> s (s z)) |] =/=
                 [lambda| \s z -> s (s (s (s (s (s (s (s (s (s (s (s (s (s (s (s z))))))))))))))) |]
    it "underLet" $
      shrinkBeta [lambda| let {
        noChange = \x y -> x y x;
        phantom = (\x y -> y) (\x y -> y x);
        multiple = (\x y -> y x) (\z -> z) (\w -> w)
      } in (\a b -> noChange b) phantom multiple
      |] === [lambda| let {
        noChange = \x y -> x y x;
        phantom = \x -> x;
        multiple = \x -> x
      } in noChange multiple
      |]
    it "underLetRec" $
      shrinkBeta [lambda| letrec {
        noChange = \x y -> x y x;
        phantom = (\x y -> y) (\x y -> y x);
        multiple = (\x y -> y x) (\z -> z) (\w -> w);
        refer = (\a b -> noChange b) phantom multiple
      } in refer
      |] === [lambda| letrec {
        noChange = \x y -> x y x;
        phantom = \y -> y;
        multiple = \x -> x;
        refer = noChange multiple
      } in refer
      |]
    it "underLetRec2" $
      shrinkBeta [lambda|
        \a b -> (\c -> (\d -> c d d) (\p -> letrec { r = a } in p r a)) b
      |] === [lambda|
        \a b -> (\d -> b d d) (\p -> letrec { r = a } in p r a)
      |]
    
    it "regression1" $
      let expr = [lambda| letrec { a = a a; b = b } in (\f g -> a) (a a) |] :: ParsedLam VarName
      in Spec.Common.shouldEquiv expr (shrinkBeta expr)
  context "sccAnalysis" $ do
    it "noChange" $
      sccAnalysis [lambda|
        letrec { a = \x -> b; b = \y -> y a } in a
      |] === [lambda|
        letrec { a = \x -> b; b = \y -> y a } in a
      |]
    it "nestedLet" $
      sccAnalysis [lambda|
        letrec { a = \x -> x; b = \x -> x a; c = \x -> x a b; d = \x -> x a c } in d
      |] === [lambda|
        let { a = \x -> x }
        in let { b = \x -> x a }
        in let { c = \x -> x a b }
        in let { d = \x -> x a c }
        in d
      |]
    it "nestedLetRec" $
      sccAnalysis [lambda|
        letrec { a = b; b = a; c = a d; d = c b } in d
      |] === [lambda|
        letrec { a = b; b = a } in letrec { c = a d; d = c b} in d
      |]
  context "floatLetOutward" $ do
    it "test1" $
      floatLetOutward [lambda|
        letrec {
          a = \x -> x;
          b = let { c = \x y -> y } in c c b
        } in a b
      |] === [lambda|
        letrec {
          a = \x -> x;
          b = c c b;
          c = \x y -> y
        } in a b
      |]
    it "test2" $
      floatLetOutward [lambda|
        \x -> letrec { a = x a } in \y -> let { b = a x } in y b
      |] === [lambda|
        \x -> letrec { a = x a; b = a x } in \y -> y b
      |]
  context "floatLetInward" $ do
    it "test1" $
      floatLetInward [lambda|
        \x -> letrec { a = x a } in \y -> let { b = a x } in a (b y b)
      |] === [lambda|
        \x -> letrec { a = x a } in \y -> a (let { b = a x } in b y b)
      |]
  context "soundConversion" $
    for_ manipulations $ \(name,f) ->
      it name $ soundConversion f
  context "typePreservation" $
    for_ manipulations $ \(name,f) ->
      it name $ typePreservation f

manipulations :: [(String, L -> L)]
manipulations =
  [("renameCollision", renameCollision)
  ,("toUniqueNames", toUniqueNames)
  ,("shrinkBeta", shrinkBeta)
  ,("sccAnalysis", sccAnalysis)
  ,("inlineLet", inlineLet)
  ,("floatLetOutward", floatLetOutward)
  ,("floatLetInward", floatLetInward)
  ]

