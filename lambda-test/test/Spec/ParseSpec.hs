{-# LANGUAGE QuasiQuotes #-}
module Spec.ParseSpec(spec) where

import Control.Monad (forM_)
import Test.Hspec
import Test.QuickCheck

import Text.Heredoc

import Text.PrettyPrint.Formattable

import Spec.Common
import Spec.Examples
import Data.Lambda.RandomGen()

spec :: Spec
spec = do
  forM_ examples $ \ex ->
    it (egName ex) $ egStr ex `shouldParsesTo` egLam ex
  forM_ parseErrExamples $ \(name, src) ->
    it name $ shouldNotParse src
  it "(parse . format) = id" $
    property $ \t -> formatL t `shouldParsesTo` t

parseErrExamples :: [(String, String)]
parseErrExamples =
  [("parseErr.1", [here| \ \x -> x |])
  ,("parseErr.2", [here| \x -> (x (\y -> y) |])
  ,("parseErr.3", [here| let { x y z } in z |])
  ]
