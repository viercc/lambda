module Data.TypeCheck.TH (
  tscheme
) where

import Data.TypeCheck.Parse

import Language.Haskell.TH.Syntax (Lift(..))
import Data.THUtil

tscheme :: QuasiQuoter
tscheme = qqParseSplice typeSchemeP lift
