{-# LANGUAGE DeriveFoldable      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveLift          #-}
{-# LANGUAGE DeriveTraversable   #-}
{-# LANGUAGE EmptyCase           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeFamilies        #-}
module Data.THUtil (
  QuasiQuoter,
  qqParseSplice,
  liftPos, liftRange
) where

import           Data.Lambda.Parse

import qualified Language.Haskell.TH           as TH
import           Language.Haskell.TH.Quote     (QuasiQuoter (..))
import           Language.Haskell.TH.Syntax    (Lift (..))
import qualified Language.Haskell.TH.Syntax    as TH

import           Text.Megaparsec

qqParseSplice ::
  (Ord e, ShowErrorComponent e) =>
  Parsec e String a -> (a -> TH.ExpQ) -> QuasiQuoter
qqParseSplice parser spliceExpr =
  QuasiQuoter { quoteExp = qLamExp,
                quotePat = \_ -> fail "Unsupported",
                quoteType = \_ -> fail "Unsupported",
                quoteDec = \_ -> fail "Unsupported" }
  where
    qLamExp quoted = do
      loc <- TH.qLocation
      let name = TH.loc_filename loc
          (line,column) = TH.loc_start loc
          parser_pos = SourcePos name (mkPos line) (mkPos column)
          parser' = setPosition parser_pos >> parser
      case parse parser' name quoted of
        Left err -> fail $ errorBundlePretty err
        Right e  -> spliceExpr e

setPosition :: (Ord e) => SourcePos -> Parsec e String ()
setPosition newPos = updateParserState $ \pst ->
  pst{ statePosState = (statePosState pst){ pstateSourcePos = newPos } }

liftRange :: SourceRange -> TH.ExpQ
liftRange (SourceRange p1 p2) = [| SourceRange $(liftPos p1) $(liftPos p2) |]
liftRange (BuiltinPos msg)    = [| BuiltinPos $(lift msg) |]

liftPos :: SourcePos -> TH.ExpQ
liftPos pos =
  let name = sourceName pos
      line = unPos $ sourceLine pos
      column = unPos $ sourceColumn pos
  in [| SourcePos $(lift name) (mkPos $(lift line))
                               (mkPos $(lift column)) |]
