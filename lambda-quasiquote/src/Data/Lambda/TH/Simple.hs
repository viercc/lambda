{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeFamilies        #-}
module Data.Lambda.TH.Simple (
  -- * Simple quoter [lambda| ... |]
  lambda,
  ParsedLam',
  -- * Customize
  QuasiQuoter,
  qqLam,
) where

import           Data.Lambda.Core
import           Data.Lambda.Parse

import qualified Language.Haskell.TH.Quote     as TH
import           Language.Haskell.TH.Syntax    (Lift (..))

import           Data.Lambda.TH.Core

qqLam :: (Lift lit) => Parser lit -> TH.QuasiQuoter
qqLam litP = qqParseSplice (lamP litP) splicer
  where
    splicer = spliceLam handleLoc handleLit spliceFV
    spliceFV x = [| fv_ $(litVarName x) |]
    handleLit a = [| lit_ $(lift a) |]
    handleLoc pos expr = [| loc_ $(liftRange pos) $expr |]

-- | Quasi quoters
type ParsedLam' = forall a. ParsedLam a

-- | [lambda| ___ |] :: ParsedLam'
lambda :: QuasiQuoter
lambda = qqLam noLiteralP
