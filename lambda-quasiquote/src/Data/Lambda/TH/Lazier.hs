{-# LANGUAGE DeriveFoldable      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveTraversable   #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeFamilies        #-}
module Data.Lambda.TH.Lazier (
  -- * Lazier quoter [lazier| ... |]
  lazier,
  Lazier, Lazier',
  ripLazier, packLazier,
  -- * Customize
  QuasiQuoter,
  qqLazier,
) where

import           Control.Monad.State           hiding (MonadTrans (..))
import qualified Data.Map.Lazy                 as Map
import           Data.Maybe                    (isJust)
import qualified Data.Set                      as Set

import           Data.String                   (fromString)

import           Text.PrettyPrint.Formattable  as PP
import           Text.PrettyPrint.Util         as PP

import           Data.Lambda.Core
import           Data.Lambda.Format (formatLamP)
import           Data.Lambda.Parse

import qualified Language.Haskell.TH           as TH
import           Language.Haskell.TH.Syntax    (Lift (..))

import           Data.Lambda.TH.Core

-- * Lazier mechanism
data LazierVar t = ReallyFreeName VarName | RefName String String t
newtype Lazier a = MkLazier (Lam VarName SourceRange a (LazierVar (Lazier a)))

type Lazier' = forall a. Lazier a

instance Formattable (LazierVar t) where
  formatPrec _ v = case v of
    ReallyFreeName x -> formatDoc x
    RefName _ shortName _ -> pretty shortName

instance Formattable a => Formattable (Lazier a) where
  formatPrec p (MkLazier e) = formatLamP p e

qqLazier :: forall a. (Lift a) => Parser a -> QuasiQuoter
qqLazier litP = qqParseSplice (lamP litP) splicer where
  splicer :: ParsedLam a -> TH.ExpQ
  splicer l = [| MkLazier $(spliceLam handleLoc handleLit handleFV l) |]

  handleLit :: a -> TH.ExpQ
  handleLit a = [| lit_ $(lift a) |]

  handleLoc :: SourceRange -> TH.ExpQ -> TH.ExpQ
  handleLoc pos e = [| loc_ $(liftRange pos) $e |]

  handleFV :: VarName -> TH.ExpQ
  handleFV x =
    let xStr = format x
    in TH.lookupValueName xStr >>= \case
      Nothing -> [| fv_ (ReallyFreeName $(litVarName x)) |]
      Just hostName ->
       if isJust (TH.nameModule hostName)
         then
           [| fv_ $ RefName
                $(lift (show hostName))
                $(lift (TH.nameBase hostName))
                $(TH.dyn xStr) |]
         else
           TH.varE hostName

-- | [lazier| ... |] :: Lazier'
lazier :: QuasiQuoter
lazier = qqLazier noLiteralP

-- | Convert LamLazier which does not include recursive definition to
--   ParsedLam by simply ripping off NamedRef.
--   (do not work when recursion exists)
ripLazier :: Lazier a -> ParsedLam a
ripLazier = go Set.empty
  where 
    go env (MkLazier t) = t >>= \case
      ReallyFreeName x -> fv_ x
      RefName hostName _ r ->
        if hostName `Set.member` env
          then err
          else go (Set.insert hostName env) r
    
    err = error "ripLazier: Recursion found. use packLazier instead."

-- | convert Lazier to ParsedLam by making all NamedRef definitions to
--   big one letrec
packLazier :: Lazier a -> ParsedLam a
packLazier t = toLetRec $ runState (go t) initialState
  where
    initialState = (Map.empty, Map.empty, newNameGenerator (const True))
    lookupName name = gets $
      \(visited, _, _) -> Map.lookup name visited
    visit name vname = modify $
      \(visited, defs, gen) -> (Map.insert name vname visited, defs, gen)
    addDef v e = modify $
      \(visited, defs, gen) -> (visited, Map.insert v e defs, gen)
    fresh base = state $
      \(visited, defs, gen) ->
         let (v, gen') = requestNameFor (fromString base) gen
         in (v, (visited, defs, gen'))
    newDef name shortName def =
      do v <- fresh shortName
         visit name v
         defE <- def
         addDef v defE
         return v
    
    go (MkLazier e) = traverse onFV e
    onFV (ReallyFreeName x) = pure x
    onFV (RefName name shortName t') = lookupName name >>= maybe (newDef name shortName (go t')) return
    
    toLetRec (body, (_, defs, _))
      | Map.null defs = body
      | otherwise     = makeLetRec (Map.toList defs) body
