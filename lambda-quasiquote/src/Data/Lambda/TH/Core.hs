{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TemplateHaskell  #-}
{-# LANGUAGE TypeFamilies     #-}
module Data.Lambda.TH.Core (
  QuasiQuoter,
  qqParseSplice,
  liftPos, liftRange,
  litVarName,
  spliceLam
) where

import           Data.String                  (fromString)

import           Text.PrettyPrint.Formattable as PP

import           Data.Lambda.Core

import qualified Language.Haskell.TH          as TH
import           Language.Haskell.TH.Syntax   (Lift (..))

import           Data.THUtil

spliceLam :: (Formattable v) =>
             (l -> TH.ExpQ -> TH.ExpQ) ->
             (a -> TH.ExpQ) ->
             (x -> TH.ExpQ) ->
             Lam v l a x -> TH.ExpQ
spliceLam handleLoc handleLit handleFreeVar = cata step where
  step ft = case ft of
    App e1 e2  -> [| app_ $e1 $e2 |]
    Abs x (Scope body) -> [| abs_ $(litVarName x) (Scope $body) |]
    Let defs (Scope body) -> [| let_ $(spliceDefs defs) (Scope $body) |]
    LetRec defs (Scope body) -> [| letrec_ $(spliceDefsS defs) (Scope $body) |]
    BV n m     -> [| bv_ $(lift n) $(lift m) |]
    FV x       -> handleFreeVar x
    Lit a      -> handleLit a
    Loc l e    -> handleLoc l e
  spliceDefs defs = TH.listE $
    map (\(x,e) -> TH.tupE [litVarName x, e]) defs
  spliceDefsS defs = TH.listE $
    map (\(x,Scope e) -> TH.tupE [litVarName x, [| Scope $e |] ]) defs
 
litVarName :: Formattable v => v -> TH.ExpQ
litVarName v = [| fromString $(lift (format v)) |]
